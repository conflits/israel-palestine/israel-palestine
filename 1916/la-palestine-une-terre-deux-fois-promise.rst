.. index::
   pair: Benjamin Barthe; La Palestine, une terre deux fois promise (2024-01-07)
   
.. _barthe_1_sur_5_2024_01_07:

======================================================================================================================================
2024-01-07 Série "Israël-Palestine, la guerre sans fin" (1/5) **La Palestine, une terre deux fois promise** par Benjamin Barthe
======================================================================================================================================

- https://www.lemonde.fr/international/article/2024/01/07/la-palestine-une-terre-deux-fois-promise_6209558_3210.html
- https://www.lemonde.fr/signataires/benjamin-barthe/

.. tags:: 1916, Lord Balfour, Antisémitisme européen, Chaïm Weizmann

ISRAËL-PALESTINE, LA GUERRE SANS FIN
=============================================

C’est le drame d’une terre saturée d’histoire et de symboles, théâtre
d’un conflit aux répercussions planétaires. De la naissance du sionisme, sur
fond de persécutions antijuives en Europe et d’agonie de l’Empire ottoman,
à la montée, au début du XXIe siècle, d’un antagonisme rendant très
improbable le rêve d’une solution à deux Etats, « Le Monde » revient,
dans une série de cinq articles produits par d’anciens correspondants dans
la région, sur l’histoire de la guerre sans fin qui oppose Israéliens
et Palestiniens.

Récit« Israël-Palestine, la guerre sans fin » (1/5)
=========================================================

En appuyant, en 1917, la revendication d’un foyer national juif en
Palestine, le Britannique Lord Balfour choisit le sionisme, un mouvement né
en réaction contre l’antisémitisme européen, ignorant les aspirations à
l’indépendance des Arabes, en lutte contre l’Empire ottoman.

La scène se déroule par une belle nuit de 1916, dans le quartier de
Westminster, à Londres. Deux gentlemen élégamment vêtus déambulent sous
la lueur pâle de la lune. Ils discutent de l’avenir de la Palestine, cette
province de l’Empire ottoman que l’armée de Sa Majesté se prépare à
conquérir, dans le prolongement de la révolte arabe galvanisée par Thomas
Edward Lawrence, dit « Lawrence d’Arabie ».

L’homme portant bouc et moustache qui mène la conversation est Chaïm
Weizmann, 42 ans, un chimiste juif de renom, professeur à l’université
de Manchester (Royaume-Uni). Hanté par les pogroms de la Russie tsariste où
il est né, celui qui deviendra le premier président de l’Etat d’Israël
dirige alors la branche britannique de l’Organisation sioniste mondiale. Ce
mouvement, lancé à la fin du XIXe siècle par un journaliste israélite
austro-hongrois, Theodor Herzl, milite pour l’établissement d’un Etat
juif en Palestine. La patrie ancestrale de cette nation fantôme, dispersée
aux quatre vents, est alors habitée par 500 000 Arabes et moins de 40 000 juifs.

L’homme qui écoute, cheveux lustrés et bacchantes grisonnantes, est
l’ancien premier ministre conservateur (1902-1905), Lord Arthur James Balfour,
alors premier lord de l’amirauté. Ironie de l’histoire, en 1905, il avait
fait voter une loi très restrictive, destinée à endiguer le flot des victimes
des persécutions antisémites d’Europe de l’Est, qui affluaient à cette
époque outre-Manche. Comme nombre de ses pairs, Balfour est pétri de préjugés
à l’égard des juifs, dont il se méfie autant qu’il les idéalise.

Grâce à un ami journaliste, Weizmann a pénétré les cercles dirigeants
britanniques. Son sésame ? Un procédé permettant de fabriquer de l’acétone
synthétique, composé indispensable à la production d’explosifs. En plein
conflit avec l’Allemagne, principal exportateur de ce solvant en Europe,
la découverte tombe à pic. En échange de sa contribution à l’effort de
guerre, le professeur Weizmann a gagné l’oreille des plus hauts responsables
de la Couronne.

Des siècles de persécutions
===================================

Cette nuit de 1916, alors qu’il arpente le pavé londonien en compagnie de Lord
Balfour, chez qui il a dîné, le leader sioniste martèle son principal argument
: les intérêts de son mouvement et du Royaume-Uni sont alignés. L’homme de
science a beau opérer à partir d’un petit appartement sombre de Piccadilly
Circus, il s’est forgé, par son charisme et son entregent, l’image d’un
« roi des Juifs ». En bons protestants, Balfour et le premier ministre,
David Lloyd George, sont imprégnés de la mythologie romantique du retour
à Sion, présentée comme le prélude à la rédemption finale, la seconde
venue du Christ sur terre.

Parallèlement à ce philosémitisme biblique, les deux hommes inclinent à
penser, comme le veut un tenace cliché antisémite, que la « race juive »
jouit d’un pouvoir d’influence occulte. Ils se persuadent ainsi que Weizmann
et les siens pourraient être utiles au Royaume-Uni. Qu’ils pourraient
hâter l’entrée en guerre des Etats-Unis aux côtés des puissances de
l’Entente et qu’en s’installant en Palestine, non loin du canal de Suez,
ils pourraient les aider à sécuriser la route des Indes. Quelque temps après
cette promenade nocturne, le secrétaire au Foreign Office déclare devant le
cabinet : « Je suis sioniste. »

Le mouvement a émergé dans les années 1880, en réponse à la multiplication
des émeutes antijuives en Russie. En 1881, le tsar Alexandre II est assassiné
par un groupe anarchiste. Aussitôt, la rumeur se répand dans l’empire que
le meurtre a été commis par des juifs, et bientôt s’y ajoute le bruit
qu’en représailles le nouveau tsar, Alexandre III, autorise que l’on batte
les israélites… Les massacres, tolérés, voire encouragés par le pouvoir,
commencent, et cette « première vague » durera jusqu’en 1884. D’autres
lui succéderont, en particulier dans les années 1890 et en 1903-1907,
faisant des dizaines de milliers de victimes et scandalisant les opinions
publiques occidentales.

En réaction contre ces violences faisant écho aux siècles d’opprobre
et de persécutions vécus en Europe par la diaspora juive, Léon Pinsker,
un médecin juif d’Odessa (aujourd’hui en Ukraine), publie, en 1882,
une brochure intitulée Autoémancipation. Convaincu que l’antisémitisme
est une pathologie incurable, que le juif sera toujours un apatride, le «
peuple élu de la haine universelle », il appelle ses coreligionnaires à
renoncer aux deux stratégies d’émancipation alors dominantes : le pari
de l’assimilation, en vogue en Europe occidentale, et la quête d’une
autonomie culturelle, qui progresse dans les cercles socialistes d’Europe
de l’Est et que l’on va bientôt appeler le « bundisme ». A la place,
Pinsker prône la création d’un « foyer national » : « Une terre à nous,
(…) un grand bout de sol pour nos pauvres frères, un bout de sol dont nous
aurions la propriété et d’où nul étranger ne puisse nous chasser. »


La Palestine, terre de l’antique royaume d’Israël et de Juda, est
le débouché naturel de cette aspiration. De nombreux juifs en ont été
chassés il y a près de deux millénaires, à la suite de l’écrasement
par les Romains de la révolte de Bar-Kokhba, en 135 après Jésus-Christ. La
province, dirigée d’Istanbul par le sultan ottoman, est peuplée à plus de
90 % d’Arabes. Mais le souvenir de la « Terre promise » irrigue toujours
la diaspora juive, comme en témoigne la formule rituelle « l’an prochain
à Jérusalem ».

Cette nostalgie et la diffusion des premiers écrits sionistes mènent à
la création du réseau des Amants de Sion, matrice de la première alya,
la « montée » vers Israël. Entre 1881 et 1890, 10 000 migrants fondent
des colonies agricoles, le long de la plaine côtière et dans les collines de
Galilée. Ces pionniers, soutenus par des philanthropes juifs, s’ajoutent aux
quelque 25 000 juifs palestiniens vivant depuis des siècles entre Jérusalem,
Tibériade, Safed et Hébron. Mais la présence juive en Palestine reste faible
et le choix de l’alya marginal : 2 millions de personnes en tout fuiront
les persécutions tsaristes entre 1880 et 1914. Les trois quarts partiront
pour l’Amérique du Nord.

La publication, en 1896, de L’Etat des juifs, le manifeste de Theodor Herzl
(1860-1904), apporte un second souffle au sionisme. Le grand bourgeois viennois,
fervent assimilationniste jusque-là, s’est converti à cette idéologie
après avoir couvert, à Paris, le premier procès du capitaine Alfred Dreyfus
et son infamante dégradation publique dans la cour de l’Ecole militaire,
le 5 janvier 1895. Herzl prône un sionisme étatique, persuadé que la
haine des juifs favorisera son entreprise. « Les gouvernements dans les pays
desquels sévit l’antisémitisme seront très intéressés à nous procurer
la souveraineté », prédit-il, sur un ton glacialement réaliste.

Rejet de l’option « Ouganda »
===================================

L’année suivante, Herzl réunit à Bâle, en Suisse, le premier
congrès sioniste mondial, qui appelle à la création d’un foyer juif en
Palestine. Dans son journal, à la date du 3 septembre 1897, il écrit : « A
Bâle, j’ai fondé l’Etat juif… dans cinq ans peut-être, dans cinquante
sûrement, chacun le verra. »

A cette époque, un autre homme fait preuve de prescience : le Palestinien
Youssef Diya Al-Khalidi, ancien maire de Jérusalem. Ce quinquagénaire
polyglotte a suivi l’essor de la pensée sioniste à travers les journaux
étrangers. Il a aussi observé les premières frictions entre les nouveaux
arrivants et les fellahs (paysans) locaux, déplacés de force pour permettre
la création des colonies. Alors, le 1er mars 1899, Youssef Diya Al-Khalidi
envoie une lettre de sept pages au grand rabbin de France, Zadoc Kahn, lui
demandant de la transmettre à Herzl.

Après avoir exprimé son respect pour le fondateur du sionisme, un « vrai
patriote juif », et reconnu les liens ancestraux des juifs avec la Palestine
– « Mon Dieu, historiquement, c’est votre pays ! » –, l’ancien édile
se fait grave : c’est « folie pure » que de vouloir bâtir un Etat juif
souverain dans ce pays qui « fait partie intégrante de l’Empire ottoman
et, ce qui est plus grave, est habité par d’autres que des israélites »,
prévient Al-Khalidi, qui redoute que ce projet ne sème la discorde. « Au
nom de Dieu, laissez la Palestine en paix », conclut-il en implorant Herzl
de trouver un autre refuge pour son peuple.

Quelques jours plus tard, l’intéressé lui répond. Il présente les juifs
comme des amis de la Turquie et des musulmans, dont l’esprit d’entreprise
profitera à l’Empire ottoman. « Qui pourrait penser à faire partir la
population non juive, demande le prophète du sionisme ? C’est son bien-être,
sa richesse individuelle, que nous allons accroître en apportant la nôtre. »

Un Etat juif dans un pays arabe ? Lord Balfour n’ignorait pas cette
contradiction, inhérente au projet sioniste. En 1903, alors qu’il était
locataire du 10 Downing Street, il avait suggéré au mouvement de reporter ses
ambitions sur l’Ouganda, dans l’est de l’Afrique. Un scénario rejeté
par les délégués du congrès juif, pour qui le sionisme sans Sion n’a
pas de sens. « Si Moïse s’était vu offrir l’Ouganda à la place de la
Palestine, il aurait brisé les Tables de la Loi », lance Herzl à Balfour.

Pour l’intellectuel viennois, imprégné d’une idée de supériorité
européenne très commune à l’époque, le sionisme participe du mouvement
d’expansion du Vieux Continent en Afrique et en Asie. Dans L’Etat des juifs,
il a écrit que cet Etat serait l’« avant-garde de la civilisation contre
la barbarie ». Dans son journal, en 1895, il a même avancé une solution
au casse-tête palestinien. « Nous devons essayer de faire disparaître la
population sans le sou de l’autre côté de la frontière, en lui procurant
des emplois dans les pays de transit », mentionnait-il, beaucoup plus franc
que dans sa réponse à Youssef Diya Al-Khalidi.

Un levier pour Londres
=============================

La sécession des provinces arabes de l’Empire ottoman, symbolisée par
la prise d’Aqaba, en juillet 1917, par Lawrence d’Arabie et les forces
de l’émir Fayçal, accélère le cours de l’histoire. Le pari sioniste
devient pour Londres un levier pour s’approprier les dépouilles de l’«
homme malade de l’Europe ». C’est alors que, le 31 octobre 1917, le cabinet
de guerre britannique appuie la fameuse déclaration Balfour, adressé à Sir
Walter Rothschild, un ancien député conservateur.

« Le gouvernement de Sa Majesté envisage favorablement l’établissement en
Palestine d’un foyer national pour les juifs et fera tout ce qui est en son
pouvoir pour faciliter la réalisation de cet objectif, étant clairement entendu
que rien ne sera fait qui puisse porter atteinte soit aux droits civiques et
religieux des collectivités non juives existant en Palestine, soit aux droits
et au statut politique dont les juifs disposent dans tout autre pays. »

Une seule phrase, quinze lignes dactylographiées, mais un chef-d’œuvre de
circonlocutions. Il s’agit de donner des garanties aux communautés en Europe,
encore très largement acquises aux idéaux assimilationnistes et bundistes,
et d’endormir la vigilance des Palestiniens. Mais ces derniers, réduits au
rang de « collectivités non juives », alors qu’ils forment l’écrasante
majorité de la population, ne sont pas dupes. « Le sionisme est le danger qui
guette notre patrie. Il annonce notre exil et notre expulsion de nos demeures
et de nos propriétés », prédit le réformiste Suleiman Al-Farouqi. « Une
nation a solennellement promis à une deuxième le territoire d’une troisième
», résumera l’écrivain juif d’origine hongroise Arthur Koestler.


Ce faisant, le Royaume-Uni a bafoué deux engagements : la promesse faite,
en 1915, au chérif Hussein de La Mecque, le père de Fayçal, d’ériger
un grand royaume arabe dans le « Bilad El-Cham », le Levant ; et l’accord
Sykes-Picot, scellé l’année suivante, un plan anglo-français de partage
du Proche-Orient, qui prévoyait l’internationalisation de la Palestine. A
ces deux pactes secrets, Londres a préféré l’option sioniste.

La diplomatie anglaise espère ainsi détacher les juifs de leur gouvernement en
Allemagne et en Autriche-Hongrie et dissuader les bolcheviks et les mencheviks
russes, qui comptent de nombreux juifs dans leurs rangs, de signer une paix
séparée avec Berlin – un calcul qui échouera. Avant tout, l’alliance avec
les sionistes, associée à l’entrée du général Allenby à Jérusalem, à
la fin de l’année 1917, permet aux Britanniques d’évincer leurs rivaux
français de la Palestine. En 1922, c’est à eux seuls que la Société
des nations confie le mandat sur ce pays. A charge pour Londres de mettre en
application la déclaration Balfour.

Deuxième alya, troisième, quatrième… Sous le regard longtemps bienveillant
des officiers de Sa Majesté, le nombre de juifs en Palestine passe de 80 000,
au début des années 1920, à plus de 600 000 en 1948, date de la création
d’Israël. Soit de 12 % à 32 % de la population totale du pays. Simple
excroissance du port arabe de Jaffa lors de sa fondation, en 1909, Tel-Aviv
dépasse les 150 000 habitants en 1937. Ce dynamisme démographique est
alimenté par la Grande Dépression, en 1929, qui ferme la route des Etats-Unis
aux juifs européens, et par l’accession d’Hitler au pouvoir en Allemagne,
en janvier 1933.

Quelques mois plus tard, l’Organisation sioniste mondiale signe avec le
gouvernement nazi l’accord dit « Haavara » (« transfert »), qui permet
aux juifs allemands rejoignant la Palestine d’y récupérer une partie de
leur capital, sous forme de produits exportés par le Reich. A cette époque,
le régime hitlérien n’en est pas encore à planifier l’extermination des
juifs d’Europe, et beaucoup de responsables occidentaux pensent qu’il est
possible de négocier avec le Führer.

Le Fonds national juif (FNJ), chargé d’acquérir des terres, excelle aussi
à dénicher des effendis (propriétaires) absents, qui résident à Damas ou
à Beyrouth, et ne se font pas trop prier pour vendre. Les Britanniques lui
cèdent des terres d’Etat, et d’autres sont confisquées à des Bédouins
ne disposant pas de titre de propriété. En vingt-cinq ans, la superficie
des terrains cultivés par les juifs est multipliée par trois.

Paupérisation des Arabes
===================================

La conquête de la terre se double d’une conquête du travail. Les paysans
palestiniens employés sur les terres acquises par le FNJ sont renvoyés au
profit de métayers juifs. La Histadrout, la centrale syndicale établie par
les immigrants, décide, en 1920, d’exclure les travailleurs non juifs de
ses rangs. Des affiches fleurissent sur les murs de Jérusalem et de Tel-Aviv,
appelant à boycotter les commerces arabes. Officiellement, ces mesures visent
à remettre au premier plan le travail manuel, en particulier le travail de la
terre, longtemps interdit aux juifs dans les pays européens. Dans la pratique,
elles paupérisent la population arabe.

L’écart entre les deux communautés se creuse, d’autant plus que la
direction palestinienne pratique la politique de la chaise vide. Elle s’oppose
à la mise en place d’un « conseil législatif unifié », composé de juifs
et d’Arabes, et d’une « agence arabe », équivalent de l’Agence juive,
dont Chaïm Weizmann a pris la direction et qui est devenue le bras exécutif
du Yichouv, la communauté juive de Palestine. Hors de question de donner le
moindre signe d’acceptation de la déclaration Balfour.

En 1929, dans un éditorial intitulé « Des étrangers dans notre propre
pays », le quotidien palestinien Falastin, installé à Jaffa, tire la
sonnette d’alarme. L’article rend compte d’une cérémonie organisée
par le Haut-Commissariat britannique à l’occasion de l’ouverture d’une
ligne de train, à laquelle seuls des juifs ont participé. « Il n’y avait
qu’un seul tarbouch [le couvre-chef traditionnel des Arabes du Levant], au
milieu de nombreux chapeaux », observe Issa Al-Issa, le directeur du journal,
qui s’inquiète de la « somnolence » des Palestiniens.

A intervalles réguliers, la colère de la population arabe dégénère en
violences intercommunautaires. Un premier pic survient en 1929. A Hébron, une
soixantaine de juifs sont massacrés par des extrémistes – certains sont
aussi sauvés par des familles arabes. A l’échelle de toute la Palestine,
les émeutes de 1929 et leur répression par les forces britanniques causent
la mort de 133 juifs et de 116 Arabes.

Le Royaume-Uni ne tire pas les leçons de ce premier embrasement. La politique
de Londres en Palestine continue d’être marquée par les présupposés
coloniaux de Lord Balfour, qui estimait, en 1919, dans une note de service,
que « le sionisme, qu’il ait tort ou raison (…), est d’une bien plus
grande importance que les désirs et les préjugés » des Arabes.


Alors que l’auteur de la plus célèbre déclaration du Proche-Orient meurt
en 1930, le rythme des arrivées de migrants dans les ports de Tel-Aviv et
d’Haïfa s’accélère. Après avoir construit des institutions, des écoles,
des banques et des usines, les sionistes se dotent d’une armée clandestine,
la Haganah. Lentement mais sûrement, le Yichouv se transforme en un Etat,
sous le regard tétanisé des Palestiniens, qui voient leur pays se dérober
sous leurs pieds.

Les 5 épisodes
======================

De la naissance du sionisme, sur fond de persécutions antijuives en Europe 
et d’agonie de l’Empire ottoman, à la montée, au début du XXIe siècle, 
d’un antagonisme rendant très improbable le rêve d’une solution à deux Etats, 
"Le Monde"revient, dans une série de cinq articles produits par d’anciens 
correspondants dans la région, sur l’histoire de la guerre sans fin qui 
oppose Israéliens et Palestiniens.


- :ref:`ÉPISODE 1 La Palestine, une terre deux fois promise <barthe_1_sur_5_2024_01_07>`
- :ref:`ÉPISODE 2 La Nakba, grande déchirure de la Palestine <barthe_2_sur_5_2024_01_08>`
- :ref:`ÉPISODE 3 A Jérusalem, en 1967, le retour du religieux <paris_3_sur_5_2024_01_09>`
- :ref:`ÉPISODE 4 De l’espoir d’Oslo aux réalités d'Hébron <paris_4_sur_5_2024_10_10>`
- :ref:`ÉPISODE 5 En Israël, de la promesse de concorde à l’incendie identitaire <smolar_5_sur_5_2024_01_12>`
