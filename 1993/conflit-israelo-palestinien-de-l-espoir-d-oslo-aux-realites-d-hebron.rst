
.. _paris_4_sur_5_2024_10_10:

==============================================================================================================================
2024-04-10 Série "Israël-Palestine, la guerre sans fin" (4/5)  **de l’espoir d’Oslo aux réalités d’Hébron** par Gilles Paris
==============================================================================================================================

- https://www.lemonde.fr/international/article/2024/01/10/conflit-israelo-palestinien-de-l-espoir-d-oslo-aux-realites-d-hebron_6210066_3210.html



Les faits« Israël-Palestine, la guerre sans fin » (4/5)
===============================================================

La conclusion des accords d’Oslo en 1993, après des mois de négociations
secrètes et sous le patronage américain, donne corps à l’espérance
d’une solution à deux Etats. Mais cette avancée ne suffit pas à instaurer
la confiance entre les deux communautés et les obstacles se multiplieront
sur le terrain, au point de réduire à néant les efforts des diplomates.


Un matin pluvieux de janvier 1997, une petite équipe de diplomates américains
s’avance dans la rue Al-Chouhada (« rue des Martyrs ») d’Hébron,
dans le sud de la Cisjordanie. Elle est conduite par Aaron David Miller,
inamovible adjoint du coordinateur spécial des Etats-Unis pour le Moyen-Orient,
Dennis Ross, qui soudain s’arrête et s’accroupit. « J’étais là,
à quatre pattes, avec un mètre-ruban, en train de calculer les différences
de largeur d’une petite rue qui était devenue un point de friction central
dans les négociations israélo-palestiniennes », raconte le diplomate dans
ses Mémoires. Ainsi l’équipe américaine s’efforçait-elle de trouver
un compromis permettant de lever un obstacle en apparence minuscule, mais sur
lequel butait l’ensemble du processus de paix ouvert par les accords d’Oslo,
un peu plus de trois ans plus tôt.


Historien de formation, Aaron David Miller connaît mieux que quiconque le
poids douloureux d’Hébron dans l’histoire israélo-palestinienne. La
grande ville de Cisjordanie qui porte le nom d’Al-Khalil, « l’Ami » en
arabe, a été le théâtre en 1929 de massacres antijuifs qui ont mis fin à
une présence multiséculaire autour du caveau des Patriarches de la Bible,
Abraham, Isaac, Jacob et Joseph. Ce lieu saint, à la fois juif, chrétien
et musulman, abrite leurs cénotaphes ainsi que ceux de leurs épouses. Les
émeutes avaient été déclenchées par des rumeurs d’attaques juives sur
les lieux saints musulmans de Jérusalem.

En mars 1968, un groupe de sionistes religieux conduit par un rabbin messianique,
Moshe Levinger, profite de la conquête de la Cisjordanie, un an plus tôt,
lors de la guerre des Six-Jours, pour amorcer un retour juif à Hébron. « La
renaissance nationale juive est plus importante que la démocratie… Aucun
gouvernement n’a l’autorité ou le droit de dire à un juif qu’il ne
peut pas s’installer sur n’importe quel morceau de la terre d’Israël
», clame cet élève de la yeshiva du Mercaz Harav, creuset d’un sionisme
religieux intégral. Il établit une tête de pont dans une base militaire
désaffectée située aux portes d’Hébron, qui devient la colonie de Kiryat
Arba, et multiplie les prises de possession de maisons au cœur de la vieille
ville, sources de tensions permanentes et souvent sanglantes.



Le 25 février 1994, cinq mois seulement après la signature des accords d’Oslo
dans la roseraie de la Maison Blanche, un colon extrémiste de Kiryat Arba,
Baruch Goldstein, abat au fusil-mitrailleur 29 Palestiniens en prière dans
la partie du caveau des Patriarches réservée aux musulmans et en blesse 125
autres. Ce drame constitue la première épreuve d’ampleur pour le processus
de paix négocié dans le secret en Norvège, qui a suspendu le temps de la
guerre après des décennies de fureur. L’enjeu est donc de taille.



Après la déroute arabe de 1967, les Palestiniens ont repris en main leur
destin. Leur mouvement national, forgé par ceux qui ont été contraints à
l’exil par la Nakba (la « catastrophe ») en 1948, a imposé son autonomie
et son représentant, Yasser Arafat, chef du Fatah – fondé au Koweït en
1959 – et devenu celui de l’Organisation de libération de la Palestine,
une décennie plus tard. Celui-ci a pu plaider sa cause à la tribune des
Nations unies, le 13 novembre 1974. Mais il n’a cessé d’essuyer des
revers sur le terrain, et son image a pâti du recours aux attentats comme aux
détournements d’avions, devenus la signature des groupes armés que compte
l’OLP, au sein de laquelle cohabitent des factions idéologiquement distinctes.

Jugés trop envahissants, ses combattants, les fedayins, ont été chassés
de Jordanie en septembre 1970. Dix ans plus tard, sous la pression militaire
israélienne de l’opération « Paix en Galilée » qui remonte jusqu’à
Beyrouth, l’histoire se répète au Liban, ravagé par une guerre civile
que les Palestiniens sont accusés d’avoir attisée.


La guerre de 1973, pourtant remportée par l’armée israélienne, a permis
aux vaincus de 1967 de laver en partie l’affront. Mais le front arabe vole en
éclats en 1978, lorsque l’Egypte signe, à Camp David, après des années
de patient travail du secrétaire d’Etat américain Henry Kissinger, une
paix séparée avec la droite nationaliste israélienne du premier ministre
Menahem Begin.


La décennie qui suit n’est pas non plus à l’avantage de Yasser
Arafat. L’homme, qui s’est présenté devant l’ONU comme un «
révolutionnaire » en lutte contre l’impérialisme et le colonialisme sioniste
– en dépit du soutien financier des monarchies conservatrices du Golfe –,
campe au sein d’un bloc soviétique en déclin.


Tournant pragmatique
========================


Au sein même de l’OLP, les dissensions sont fortes. La Charte maximaliste
de l’organisation, adoptée en 1964, ne conçoit un Etat palestinien que
sur la totalité du territoire dont le Royaume-Uni avait obtenu le mandat
en 1920. Mais un objectif « intermédiaire » s’esquisse lentement :
l’instauration d’un pouvoir indépendant sur toute partie de cette terre
dont Israël se serait retiré. Les communistes palestiniens défendent de
longue date cette perspective, conformément au vote de l’URSS en faveur du
plan de partage de l’ONU, en 1947.

Ce tournant pragmatique est formalisé à Alger lors du Conseil national
palestinien (CNP) de novembre 1988, où le mouvement national palestinien
se résigne à accepter les résolutions 242 et 338 des Nations unies. Ces
résolutions n’appellent qu’au retrait israélien des territoires
conquis par la guerre en 1967, soit, dans le cas palestinien, de Gaza et de
la Cisjordanie. Le CNP reconnaît ainsi implicitement un partage de la terre,
et donc la légitimité d’Israël sur la partie qui était alors la sienne.

Un mois plus tard, à Genève, Yasser Arafat annonce que l’OLP renonce
au terrorisme, dernière condition fixée par les Etats-Unis pour ouvrir un
dialogue avec la direction palestinienne. Le 2 mai, lors d’un entretien à
l’Elysée avec François Mitterrand, Yasser Arafat assure que la Charte de
1964 est désormais « caduque ». La trouvaille linguistique matérialise
cette rupture.


Un événement a précipité cette mue. En décembre 1987, l’éclatement de
la première Intifada à Gaza, qui se propage très vite à la Cisjordanie,
a pris de court l’OLP. Pour la première fois, les Palestiniens « de
l’intérieur » sont en première ligne, et gagnent la bataille des images
face à une puissante armée d’occupation, renversant à leur profit la
parabole biblique du combat entre David et Goliath. Ils contraignent l’Etat
hébreu à reconnaître la vanité du statu quo. La « révolte des pierres
» balaie en effet l’illusion d’une occupation normalisée, après les
tentatives infructueuses pour briser le mouvement national palestinien en
s’appuyant sur des notables ou des mouvements islamistes encore principalement
quiétistes, comme celui dont naîtra le Hamas, quelques jours après le début
du soulèvement.

Washington suit ces évolutions avec attention. Son intervention dans le Golfe
en 1991 pour libérer le Koweït de l’invasion irakienne, sous mandat de
l’ONU et après la constitution d’une vaste coalition étendue à des
pays arabes, a pour effet de couper l’OLP, soutien de Saddam Hussein, de
ses bailleurs de fonds du Golfe, qui prennent fait et cause pour l’émirat
agressé. Le président George H. W. Bush saisit l’occasion et met sur pied
une conférence internationale pour « mettre fin au conflit israélo-arabe »,
qui se tient à Madrid, du 30 octobre au 1er novembre 1991.


La conférence ne débouche sur aucune percée diplomatique entre Israël et le
Liban, la Syrie, la Jordanie et les Palestiniens, mais la victoire en Israël
du Parti travailliste, conduit par Yitzhak Rabin aux législatives de 1992,
rouvre le jeu. La nouvelle majorité se décide à engager secrètement des
négociations directes avec la direction palestinienne.


Deux profonds malentendus
===============================

Le 13 septembre 1993, le diplomate Aaron David Miller compte parmi les témoins
de la poignée de main historique échangée par Yitzhak Rabin et Yasser Arafat
sous les yeux du président américain démocrate Bill Clinton. Un premier
ministre israélien encore auréolé d’une brillante carrière militaire, un
chef incontesté du mouvement national palestinien, un président d’une «
hyperpuissance » sans rivale bien décidé à entrer dans l’histoire avec
un accord de paix au Proche-Orient, comme son prédécesseur démocrate Jimmy
Carter : tout semble en place pour que la période intérimaire de cinq ans
qui s’ouvre conduise à la création d’une Palestine au côté d’Israël.


Le processus d’Oslo est rejeté par la droite nationaliste et sioniste
religieuse israélienne comme par de puissantes factions palestiniennes
d’inspiration marxiste et islamiste, comme le Hamas. Il repose surtout
sur deux profonds malentendus. En reconnaissant Israël, tout d’abord
implicitement en 1988, puis explicitement en 1993, Yasser Arafat s’est
résigné à instaurer un Etat sur seulement 22 % de la Palestine mandataire,
mais il entend bien recouvrer 100 % de Gaza et de la Cisjordanie. Après tout,
Israël a cédé la totalité du Sinaï pour le prix de la paix avec l’Egypte,
et les négociations qui s’ouvrent au même moment avec la Syrie de Hafez
Al-Assad partent également du principe d’un retrait israélien intégral
du plateau syrien du Golan. L’icône du mouvement national palestinien ne
peut accepter moins concernant les territoires palestiniens conquis en 1967.


Il en va tout autrement du point de vue israélien. La poursuite de la
colonisation à Gaza et surtout en Cisjordanie, y compris dans la partie
orientale de Jérusalem, laisse entendre que ces 22 % sont également matière à
discussion. Un autre malentendu concerne le rôle de Yasser Arafat, à la tête
de l’Autorité palestinienne créée dans les territoires occupés depuis 1967.


Israël entend qu’il réprime d’une main de fer les Palestiniens opposés à
Oslo, à commencer par le Hamas. Or le chef de l’OLP a toujours régné sur
les factions palestiniennes par la cooptation et le débauchage individuel,
jamais par la manière forte. Tout en rejetant désormais la violence, il
continue de considérer qu’elle peut lui être ponctuellement utile. Il
constate enfin que ses interlocuteurs israéliens semblent plus pressés de
conclure un accord de paix avec la Jordanie, comme c’est le cas en 1994,
ou encore avec la Syrie qu’avec lui, ce qui accroît la défiance.


Deux ans après la cérémonie de Washington, un premier bilan d’étape met
en évidence des faiblesses. Les retards ne cessent de s’accumuler pour les
retraits israéliens prévus en Cisjordanie, après ceux qui ont concerné Gaza
et les principales villes palestiniennes (à l’exception de celui d’Hébron,
gelé après le massacre de 1994). Les attentats sanglants du Hamas font douter
le « camp de la paix » israélien, sur lequel s’appuie Yitzhak Rabin. C’est
alors qu’un extrémiste juif assassine le premier ministre travailliste, le
4 novembre 1995. Le processus d’Oslo ne s’en relèvera jamais tout à fait.


La mécanique laborieusement mise en place s’enraie d’autant plus que
la droite nationaliste israélienne, conduite par Benyamin Nétanyahou,
revient au pouvoir en 1996. Washington met tout en œuvre pour sauver cette
entreprise diplomatique, comme en atteste l’équipée hébronite d’Aaron
David Miller. Elle est en soi un révélateur. « Une fois sur leur terrain, la
puissante Amérique ne l’était pas tant que cela. Indépendamment de ce que
nous voulions, nos interlocuteurs avaient des calendriers, des agendas et des
intérêts qui leur étaient propres. Nous pouvions les presser, les cajoler,
voire les menacer d’abandonner, mais si nous voulions négocier des accords,
nous devions obtenir leur coopération. Et parfois, pour y parvenir, nous
devions supporter les indignités et les machinations de petites puissances
particulièrement habiles à manipuler les grandes », écrit-il quelques
années plus tard.


Partage d’Hébron en deux zones
===================================


Un accord est finalement trouvé au forceps à Hébron, avec le partage
de la ville en deux zones. La première (H1), qui concentre la majorité
des Palestiniens, soit 115 000 personnes, est placée sous le contrôle de
l’Autorité palestinienne. Israël conserve le sien sur la seconde (H2), où
résident 35 000 Palestiniens soumis à de sévères restrictions du fait de la
présence de 500 colons extrémistes, le tout sous le regard d’observateurs
internationaux. Les faits sur lesquels le sionisme religieux a bâti son projet
messianique continuent de s’imposer. Un autre accord d’étape prévoyant des
retraits israéliens additionnels en Cisjordanie est conclu en 1998, à Wye River
(Maryland), mais il reste lettre morte. Le processus tourne désormais à vide.

Le retour d’un travailliste au pouvoir en Israël sonne comme une ultime
occasion. Soldat le plus décoré d’Israël, Ehoud Barak entend profiter
des derniers mois du second mandat de Bill Clinton pour relancer Oslo. A sa
manière. Chef d’état-major de l’armée israélienne en 1993, il a fait
part, à l’époque, de ses réserves à propos des premiers accords qui,
selon lui, « comptent plus de trous que de gruyère ».

En position de force à son arrivée au pouvoir, en juillet 1999, il
brouille cependant son message en cherchant tout d’abord un accord avec un
Hafez Al-Assad affaibli par la maladie, sans tenir compte des lignes rouges
territoriales formulées par la Syrie. Il pousse à l’organisation d’une
rencontre avec le dictateur syrien à Genève, en mars 2000, qui se solde par
un échec aussi cuisant que prévisible.


En perte de vitesse dans l’opinion israélienne, Ehoud Barak reprend la piste
d’un accord avec les Palestiniens, mais à ses conditions, c’est-à-dire
sans nouer une relation de confiance avec Yasser Arafat, ni effectuer les
retraits israéliens en souffrance depuis déjà des années, et pour lesquels
il s’est pourtant engagé à Charm El-Cheikh (Egypte), en décembre 1999. A
nouveau, il force la main de Washington en précipitant des négociations sur
les questions les plus délicates laissées en souffrance : les frontières,
le sort des réfugiés palestiniens et celui de Jérusalem.


Les délégations israélienne et palestinienne se retrouvent une nouvelle
fois dans le Maryland, à Camp David, le 11 juillet 2000, en compagnie de Bill
Clinton. Une scène dit tout de l’état d’esprit de ses protagonistes. Après
une brève promenade dans les sous-bois et une pose pour les photographes,
le trio se présente devant l’entrée du bâtiment où il doit engager les
premières discussions. Chacun s’efface devant l’autre, jusqu’à ce
qu’Ehoud Barak pousse littéralement Yasser Arafat à l’intérieur en
dépit de sa résistance.


Le chef de l’OLP s’est en effet rendu à Camp David à reculons,
excédé par les manières de son interlocuteur israélien. Le manque de
préparation et surtout l’absence d’un texte de cadrage plongent très
vite les négociateurs dans une impasse, accentuée par l’immobilisme de
Yasser Arafat. Les blocages se cristallisent sur le sort de Jérusalem, et
plus précisément la souveraineté sur la Vieille Ville. Faute de contacts
préalables avec les principales capitales arabes, le chef palestinien se
refuse à la moindre concession. Les efforts déployés par le président
des Etats-Unis, jouant alternativement de la pression et du charme, n’y
changent rien. « L’empathie ne suffisait pas, analyse a posteriori Aaron
David Miller. Clinton n’avait pas la sournoiserie de Kissinger, ni l’esprit
missionnaire de Carter, ni la fermeté non sentimentale de Baker [le secrétaire
d’Etat de George H. W. Bush]. »


Le 24 juillet, l’échec est acté par un communiqué laconique. Les
Israéliens et les Américains en rejettent vite la responsabilité sur Yasser
Arafat. L’espoir né avec Oslo a vécu. Une seconde Intifada, bien plus
sanglante que la première, éclate après la visite en septembre du chef
de la droite nationaliste israélienne, Ariel Sharon, sur l’esplanade des
Mosquées, à Jérusalem. Bill Clinton quitte la Maison Blanche en janvier
2001, Ehoud Barak est défait dans les urnes deux mois plus tard. Assiégé
plus de deux ans à Ramallah, Yasser Arafat meurt de maladie, en France,
où il a été évacué en urgence, en novembre 2004.

De guerre lasse, Aaron David Miller avait quitté la diplomatie un an plus tôt.

Les 5 épisodes
======================

De la naissance du sionisme, sur fond de persécutions antijuives en Europe 
et d’agonie de l’Empire ottoman, à la montée, au début du XXIe siècle, 
d’un antagonisme rendant très improbable le rêve d’une solution à deux Etats, 
"Le Monde"revient, dans une série de cinq articles produits par d’anciens 
correspondants dans la région, sur l’histoire de la guerre sans fin qui 
oppose Israéliens et Palestiniens.


- :ref:`ÉPISODE 1 La Palestine, une terre deux fois promise <barthe_1_sur_5_2024_01_07>`
- :ref:`ÉPISODE 2 La Nakba, grande déchirure de la Palestine <barthe_2_sur_5_2024_01_08>`
- :ref:`ÉPISODE 3 A Jérusalem, en 1967, le retour du religieux <paris_3_sur_5_2024_01_09>`
- :ref:`ÉPISODE 4 De l’espoir d’Oslo aux réalités d'Hébron <paris_4_sur_5_2024_10_10>`
- :ref:`ÉPISODE 5 En Israël, de la promesse de concorde à l’incendie identitaire <smolar_5_sur_5_2024_01_12>`
