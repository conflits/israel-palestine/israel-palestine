.. _sphx_tag_lord-balfour:

My tags: Lord Balfour
#####################

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../1916/la-palestine-une-terre-deux-fois-promise.rst
