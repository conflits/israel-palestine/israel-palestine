.. _sphx_tag_nakba:

My tags: Nakba
##############

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../1948/la-nakba-grande-dechirure-de-la-palestine.rst
