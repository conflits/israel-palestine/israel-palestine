.. index::
   pair: Livre ; Israël-Palestine une terre pour deux (2024-09)

.. _livre_israel_palestine_2024:

==========================================================================================
2024-09 **Israël-Palestine une terre pour deux** par Gérard Dhotel et Véronique Corgibet
==========================================================================================

- https://www.actes-sud.fr/catalogue/israel-palestine-une-terre-pour-deux


.. figure:: images/recto.webp


Description
=============

Nouvelle édition d'un livre indispensable pour comprendre le conflit israélo palestinien.
