.. index::
   pair: Barthe; La Nakba, grande déchirure de la Palestine (2024-01-08)

.. _barthe_2_sur_5_2024_01_08:

====================================================================================================================================
2024-01-08 Série "Israël-Palestine, la guerre sans fin" (2/5) **La Nakba, grande déchirure de la Palestine** Par Benjamin Barthe 
====================================================================================================================================

- https://www.lemonde.fr/international/article/2024/01/08/la-nakba-grande-dechirure-de-la-palestine_6209690_3210.html
- https://www.lemonde.fr/signataires/benjamin-barthe/

.. tags:: 1948, Nakba

« Israël-Palestine, la guerre sans fin » (2/5).
====================================================


En 1947, deux ans après la découverte des camps d’extermination nazis,
l’Assemblée générale de l’ONU adopte un plan de partage de la Palestine
qui offre aux juifs du monde entier une terre. C’est le point de départ
d’une escalade géopolitique qui conduira à une guerre israélo-arabe ainsi
qu’à l’expulsion de centaines de milliers de Palestiniens.

Il est 10 h 30, le 21 avril 1948, quand le capitaine Amin Ezzedine, chargé de la
défense des quartiers arabes de Haïfa, pénètre, la peur au ventre, dans le
bureau du général Hugh Stockwell, commandant des forces britanniques dans le
nord de la Palestine. L’officier libanais, cadre de l’Armée de libération
arabe, une force irrégulière venue se battre au côté des Palestiniens,
redoute que ce rendez-vous ne scelle le sort de la cité portuaire, cible depuis
cinq mois d’une campagne de terreur orchestrée par les milices sionistes.

Sur son chemin, Amin Ezzedine a entendu les appels par haut-parleurs du maire
juif de la ville, Shabtai Levy (1876-1956), suppliant la population arabe,
un peu moins de la moitié des 150 000 habitants, de rester.

Mais il a aussi entendu les messages de la Haganah, la future armée
israélienne, incitant les femmes et les enfants de la communauté palestinienne
à s’enfuir « avant qu’il ne soit trop tard ». Et il sait que des camions
sillonnent les quartiers arabes en diffusant une bande-son de cris, de pleurs
et de deux mots qui saisissent d’effroi les passants : « Deir Yassine ».

Deux semaines plus tôt, le 9 avril, une centaine d’habitants de ce village
des environs de Jérusalem ont été exécutés par des miliciens de l’Irgoun,
un groupe armé d’extrême droite sioniste.

Le sang appelant le sang, un bus de médecins et d’infirmiers juifs a été
attaqué quatre jours plus tard alors qu’il faisait route vers l’hôpital
Hadassah, dans la partie est de Jérusalem. Bilan : 77 morts.

La Palestine a basculé dans la guerre civile au lendemain du 29 novembre
1947, date de l’adoption par l’Assemblée générale de l’ONU du plan
de partage de ce pays en deux Etats, l’un juif, l’autre arabe.

C’est un jour de fête pour les sionistes, qui sont alors 600 000 sur cette
terre, le résultat d’un lent processus d’immigration entamé à la fin du
XIXe siècle à la suite des pogroms perpétrés dans la Russie tsariste. Le
vote des Nations unies valide leur aspiration de toujours, la création d’un
Etat-refuge sur la terre de leurs lointains ancêtres. Un combat auquel les
6 millions de victimes de la Shoah ont apporté une légitimité tragique aux
yeux de l’opinion publique occidentale.


Vite dépassés
===================

Mais pour les Palestiniens, qui ont rejeté le plan onusien, ce 29 novembre
est un jour de rage. Ils ont le sentiment d’être dépossédés de la patrie
où ils vivent depuis des siècles. Leur dépit est d’autant plus fort
que l’Etat que leur octroie l’ONU ne recouvre que 44 % de la superficie
de la Palestine alors qu’ils représentent les deux tiers (1,2 million)
de sa population. Les juifs ont obtenu 56 % du territoire, hors la zone de
Jérusalem-Bethléem censée être internationalisée.

A Haïfa, où subsistait un fragile idéal de coopération judéo-arabe, les
affrontements éclatent le 30 décembre. Six ouvriers arabes de la raffinerie
périssent dans l’explosion d’une bombe jetée par l’Irgoun. En
représailles, trente-neuf de leurs collègues juifs sont battus à mort. Le
lendemain, la Haganah se venge sur le village voisin de Balad Al-Cheikh,
où elle laisse une soixantaine de cadavres.

Vite dépassés par leurs adversaires, abandonnés par leur élite politique
et commerçante, partie se réfugier au Liban, les Palestiniens se sont
retranchés dans la ville basse, autour du port. Ils essuient régulièrement
des attaques venues des secteurs juifs, situés en surplomb : pilonnage au
mortier, tirs de snipers, lâchers de barils explosifs, rivières de fioul
enflammé dévalant les pentes, etc. Ceux qui tentent de les éteindre sont
fauchés à la mitrailleuse.  Débâcle

Le 21 avril, le général Stockwell confirme les pires craintes du chef de
l’Armée de libération arabe. Le représentant du mandat britannique,
dont le terme est fixé au 14 mai, lui annonce le retrait immédiat de ses
troupes, qui servaient jusqu’ici de tampon avec les forces juives. La panique
s’empare aussitôt des quartiers arabes. Une foule se rue vers les quais,
sous le fracas des mortiers, dans l’espoir de fuir par la mer.

« Les hommes marchaient sur leurs amis, les femmes sur leurs propres enfants,
affirme un compte rendu de l’époque. Les bateaux dans le port furent très
vite remplis d’une cargaison vivante. La surcharge était horrible. Beaucoup
ont chaviré et coulé avec tous leurs passagers. » En l’espace de quelques
jours, la ville se vide de plusieurs dizaines de milliers de personnes. Quand
l’exode se termine, il ne reste plus que 5 000 Palestiniens à Haïfa.

Harcèlement militaire, occupation et expulsion : ce scénario se répète
durant le printemps à Tibériade, Safed, Jérusalem, Saint-Jean-d’Acre
et Jaffa, les principaux centres urbains palestiniens avec Haïfa. Pendant
ce temps, la Haganah lève le siège imposé quelques semaines aux quartiers
juifs de Jérusalem, le seul moment où la situation a été critique pour le
camp juif. Quand la création d’Israël est proclamée, le 14 mai 1948, par
David Ben Gourion (1886-1973), le chef de l’Agence juive – l’exécutif de
l’organisation sioniste mondiale en Palestine –, 350 000 Palestiniens ont
déjà été chassés de leur terre. C’est la première phase de la Nakba («
catastrophe »), un terme forgé à l’été 1948 par l’intellectuel syrien
Constantin Zureik (1909-2000), pour désigner la destruction de la Palestine.

Dans cette débâcle, les Palestiniens paient les arriérés de la
guerre précédente : la grande révolte de 1936-1939 écrasée par les
Britanniques. Partis et syndicats dissous, groupes armés laminés, leaders
politiques déportés : la défaite a porté un coup terrible au camp
palestinien, déjà affaibli par la rivalité entre le clan des Husseini,
adepte d’un nationalisme intransigeant, et celui des Nashashibi, proches
d’Abdallah Ier (1882-1951), le roi de Transjordanie.  Légions SS musulmanes

Il y a plus grave : forcé à l’exil par les Britanniques, le mufti de
Jérusalem, Hadj Amin Al-Husseini (1895-1974), a cédé aux sirènes du
régime nazi qui misait alors sur le ressentiment des Arabes à l’égard
des Britanniques pour s’implanter au Proche-Orient. Il se réfugie à
Berlin en 1941, aidant Hitler à mettre sur pied deux légions SS musulmanes,
majoritairement composées de soldats bosniaques. « Les Arabes sont les amis
naturels de l’Allemagne parce qu’ils ont les mêmes ennemis, les Anglais,
les juifs et les communistes », déclare le mufti lors d’une entrevue avec
Hitler, en novembre 1941.

A ce naufrage personnel se sont ajoutées des erreurs tactiques : en 1947,
lorsque les Nations unies se sont emparées du brûlant dossier palestinien,
à la demande de Londres, qui n’a plus les moyens d’entretenir son empire
et qui est débordé par les attentats de l’Irgoun (explosion de l’hôtel
King David, en juillet 1946), les Palestiniens ont boycotté les débats. Seuls
à s’exprimer devant les délégations, les émissaires de l’Agence juive
se sont attiré les faveurs des Etats-Unis et de l’URSS, qui voteront oui
au plan de partage, le 29 novembre.

C’est l’époque où des centaines de milliers de survivants des camps de la
mort voient dans la Palestine leur seule chance de salut. Dans un vaste espace
est-européen allant de la Pologne à la Russie soviétique, en passant par les
pays baltes, la Hongrie, les Balkans et la Roumanie, les communautés juives
ont été exterminées. Le Yiddishland tout entier a été anéanti. Comment
continuer à vivre après ce génocide ? Les Etats-Unis ne délivrent des visas
qu’au compte-gouttes et l’idée de revenir dans leur pays d’origine,
parfois ancien auxiliaire de la barbarie nazie, est insupportable à beaucoup
d’entre eux.

Ceux qui, malgré tout, tentent de retourner là où ils vivaient avant la
Shoah se heurtent à l’hostilité de leurs anciens voisins, notamment en
Pologne, où 3 millions de juifs, soit 90 % de la population juive locale,
ont disparu, et où les survivants ont été dépossédés de tout. A Kielce,
le 4 juillet 1946, 42 juifs sont tués après une émeute provoquée par des
rumeurs d’enlèvements d’enfants et de crimes rituels. Ces accusations,
relevant de fantasmes antijuifs vieux de plusieurs siècles, conduisent
de nombreux rescapés, jusqu’alors bien loin de l’idéal sioniste, au
constat dressé par Theodor Herzl (1860-1904) un demi-siècle plus tôt :
l’antisémitisme est un mal incurable, et seul un Etat pourra assurer la
sécurité du peuple juif.  Calvaire

En Palestine, la couronne britannique tente de s’accrocher à son Livre
blanc de 1939, qui visait à contenir l’immigration juive pour maintenir un
semblant de paix civile. Mais au sortir de la guerre, cette politique devient
intenable. Le calvaire enduré par les 4 500 survivants de la Shoah entassés à
bord de l’Exodus, un bateau à vapeur parti de Sète (Hérault) le 11 juillet
1947, puis refoulés du port de Haïfa et renvoyés à Hambourg en Allemagne,
bouleverse les opinions publiques.

« Qui veut et peut garantir que ce qui nous est arrivé en Europe ne
se reproduira pas ?, s’écrie peu après David Ben Gourion devant les
enquêteurs de l’ONU, venus en Palestine. Il n’y a qu’une sauvegarde :
une patrie et un Etat. »

Mais sur quel territoire et avec quelles frontières ? La déclaration
d’indépendance du 14 mai cette année-là ne le spécifiait pas. Chef de
gouvernement, David Ben Gourion veut profiter du brouillard de la guerre,
qui s’épaissit le lendemain, avec l’entrée en Palestine de troupes
de cinq Etats arabes (Egypte, Syrie, Transjordanie, Liban, Irak), pour
continuer à étendre la superficie de son Etat. Coup de poker ? Pas tant que
ça. Quelques jours avant le vote de l’ONU, l’Agence juive a conclu un
pacte secret avec Abdallah Ier : son armée, la Légion arabe, la seule force
du Proche-Orient capable de tenir tête aux sionistes, n’attaquera pas le
territoire réservé à Israël. En contrepartie, le monarque hachémite pourra
annexer la Cisjordanie.Forte de cet engagement, la jeune armée israélienne
s’attaque, en juillet 1948, à Ramleh et Lydda, deux villes attribuées par
l’ONU aux Palestiniens, de même que l’était Jaffa. Les assaillants ne
font pas de quartier. « Pratiquement tout ce qui se trouvait sur leur passage
mourait », écrit alors Keith Wheeler, un journaliste du Chicago Sun-Times. Au
bout de quelques jours, Lydda hisse le drapeau blanc et Ramleh, déjà victime
des attentats de l’Irgoun, se rend peu après.

Une interminable colonne de femmes, d’enfants, d’hommes et de vieillards se
forme aussitôt, qui prend le chemin de la Cisjordanie, où la Légion arabe
s’est déployée comme prévu. Une marche sans eau ni nourriture, fatale à
de nombreux déplacés. Les opérations de Tihour (purification), le terme
employé par l’état-major israélien, se poursuivent durant l’été et
l’automne dans les collines de Galilée. Commandée par un ancien officier
ottoman sans envergure, Fawzi Al-Qawuqji, l’Armée de libération arabe
est incapable de s’opposer au dépeuplement des villages arabes. La cité
biblique de Nazareth est épargnée à la demande de David Ben Gourion, qui
s’inquiète des réactions du monde chrétien occidental.

La dernière vague d’expulsions, à l’automne 1948, vise le désert du
Néguev. La déroute de l’armée égyptienne, encerclée dans la poche de
Falouja, facilite la conquête de Beersheba. Durant les six premiers mois
de l’année 1949, Israël signe des armistices avec chacun de ses voisins
arabes. En un an et demi, 15 000 Palestiniens ont été tués et 700 000,
plus de la moitié de la population d’avant-guerre, déracinés. Plus de
500 villages ont été détruits et 11 villes et quartiers purgés de leurs
habitants. En réaction, des émeutes antijuives éclatent au Maghreb et dans
des pays du Proche-Orient. Ces violences, couplées au pouvoir d’attraction
du nouvel Etat juif et au travail de recrutement mené par les émissaires de
l’Agence juive, vont mener, en l’espace d’une quinzaine d’années,
au départ de la presque totalité des juifs du monde arabe – environ 800
000 personnes, dont les trois quarts s’installeront en Israël.

Le jeune Etat a perdu 6 000 hommes dans les combats, mais sa victoire
est écrasante. Sa superficie est passée de 56 % à 78 % de la Palestine
mandataire. Il contrôle désormais Jérusalem-Ouest, la totalité de la
Galilée et tout le littoral, à l’exception de la bande de Gaza. Quelque
150 000 Palestiniens résident sur son territoire, alors que, dans le plan de
l’ONU, il était prévu que les juifs cohabitent avec une très importante
minorité arabe, de 400 000 personnes. Auprès de James Grover McDonald,
le premier ambassadeur américain à Tel-Aviv, Chaïm Heizmann (1874-1952),
qui s’apprête à devenir président du pays, se félicite d’« une
simplification miraculeuse des tâches d’Israël ».

Pendant plus de trente ans, cette thèse a prévalu dans le monde occidental. Les
Palestiniens sont partis de leur plein gré, dit-on alors, obéissant aux appels
des leaders arabes, leur promettant un rapide retour après la victoire. Leur
fuite serait le simple résultat des aléas de la guerre : un tragique concours
de circonstances, où se mêlent la trahison des dirigeants palestiniens et la
victoire inespérée du David israélien sur le Goliath arabe. Les travaux de
l’historien palestinien Walid Khalidi, qui a parlé, dès les années 1960,
d’une expulsion planifiée, ont été ignorés.  « Nettoyage ethnique »

Il a fallu attendre la fin des années 1980 et l’émergence du courant des «
nouveaux historiens israéliens » pour que cette thèse s’impose. Archives
à l’appui, Benny Morris, Tom Segev, Simha Flapan, Avi Shlaïm et Ilan
Pappé ont mis à bas quatre mythes : celui de la supériorité militaire
du camp pro-arabe, qui n’a en fait jamais pu rivaliser ; celui des fameux
appels radiophoniques à évacuer, qui n’ont jamais existé ; celui de
la défection des élites palestiniennes, qui a joué un rôle secondaire ;
et le plus important de tous peut être, celui de l’expulsion accidentelle.

A cet égard, une réunion joue un rôle-clé. Elle se déroule le 10 mars
1948, à la Maison rouge, un élégant bâtiment du nord de Tel-Aviv, quartier
général de la Haganah. Une dizaine de dirigeants sionistes historiques et de
jeunes officiers sont rassemblés autour de David Ben Gourion. Ils mettent la
dernière main à un document militaire secret, baptisé le « plan Daleth »,
qui va être distribué aux unités combattantes. Le texte appelle notamment,
pour « consolider l’appareil de défense », à prendre le contrôle des «
centres de population ennemie » soit « en y mettant le feu, en les dynamitant
et en déposant des mines dans leurs débris », pour ce qui est des zones les
plus hostiles, soit « en montant des opérations de ratissage et de contrôle
». « En cas de résistance, précise le document, la force armée doit être
anéantie et la population expulsée hors des frontières de l’Etat. »

Les troupes disposent aussi, pour chaque village qu’elles sont chargées
d’investir, d’un dossier recensant une multitude d’informations cruciales
: le dessin des voies d’accès, l’affiliation politique des habitants,
leurs sources de revenus, le nombre d’armes à leur disposition et, surtout,
le nom des personnes jugées hostiles, notamment celles ayant participé à
la grande révolte de 1936. Une fois la localité conquise, celles-ci sont
souvent abattues sur place.

Outre Deir Yassine, de nombreux villages sont le théâtre de massacres. C’est
le cas dans deux localités aujourd’hui disparues, Tantoura, en mai 1948,
et Dawaimeh en octobre de cette même année, où, selon Benny Morris, des
cas d’enfants au crâne fracassé et de femmes violées et brûlées vives
ont été rapportés par les soldats israéliens eux-mêmes. Des atrocités
telles qu’en novembre le ministre de l’agriculture, Aharon Zisling,
s’en émeut auprès de David Ben Gourion : « Je n’ai pas pu dormir de la
nuit. Maintenant les juifs aussi se conduisent comme des nazis et mon être
entier en est ébranlé. »

Sources : F. Encel, Atlas géopolitique d’Israël, Autrement,
2023 ; N. Weinstock, Terre promise, trop promise. Genèse du conflit
israélo-palestinien (1882-1948), Odile Jacob, 2011 ; C. Grataloup, Atlas
historique mondial, Les Arènes, 2023 ; Palestinian Academic Society for the
Study of International Affairs ; ONU

Pour Ilan Pappé, historien israélien engagé à l’extrême gauche,
comme pour son confrère Benny Morris, qui a viré progressivement à droite
au début des années 2000, tous ces éléments sont constitutifs d’un «
nettoyage ethnique ». L’idée a germé très vite parmi les théoriciens du
sionisme. Ceux-ci ont compris, intuitivement, qu’il ne serait pas possible
de construire un Etat à majorité juive dans un pays à majorité arabe, sans
se débarrasser d’une grande partie de cette population. « Nous pensons que
la colonisation de la Palestine doit aller dans deux directions : installation
des juifs en Eretz Israël et réinstallation des Arabes d’Eretz Israël en
dehors du pays », écrivait en 1917 Aryé-Yéhouda-Léo Motzkin, l’un des
penseurs les plus libéraux du mouvement sioniste.  « Chassez-les »

Le procédé étant considéré comme moralement douteux, les dirigeants
sionistes n’en parlaient qu’en cercle fermé, en utilisant l’euphémisme
« transfert ». « La force juive grandit et elle renforcera aussi nos
possibilités de réaliser le transfert à une grande échelle », se
réjouissait, en 1937, David Ben Gourion dans un discours ultérieurement
« caviardé ». Lui-même n’a pas laissé d’ordre écrit en ce sens,
précise Benny Morris, qui, au début de ses recherches, minimisait le caractère
centralisé de la politique d’expulsion et l’importance du plan Daleth. Mais,
comme il l’a confié en 2004 au quotidien israélien Haaretz, en 1948, «
l’idée du transfert est dans l’air. Le corps des officiers comprend ce
qui est attendu d’eux ».

L’exemple le plus frappant est l’attitude du premier ministre à
Ramleh. Quand le commandant de l’attaque, Yigal Allon (1918-1980), lui demande
ce qu’il faut faire des habitants, David Ben Gourion répond par un geste de
la main signifiant : « Chassez-les. » Alors adjoint de Yigal Allon, Yitzhak
Rabin (1922-1995), le futur premier ministre israélien, racontera la scène dans
un passage censuré de ses Mémoires, dévoilé en 1979 par le New York Times.

Dans le courant de l’année 1948, pour abriter les réfugiés palestiniens
qui affluent en Cisjordanie et à Gaza ainsi qu’en Syrie, en Jordanie et au
Liban, des dizaines de camps de toile sortent de terre. L’administration de
ces lieux est confiée l’année suivante à l’Office de secours et de travaux
des Nations unies. C’est là que va grandir la génération de la Nakba, dans
la nostalgie d’un monde perdu et la volonté de se battre pour le reconquérir.

Les 5 épisodes
======================

De la naissance du sionisme, sur fond de persécutions antijuives en Europe 
et d’agonie de l’Empire ottoman, à la montée, au début du XXIe siècle, 
d’un antagonisme rendant très improbable le rêve d’une solution à deux Etats, 
"Le Monde"revient, dans une série de cinq articles produits par d’anciens 
correspondants dans la région, sur l’histoire de la guerre sans fin qui 
oppose Israéliens et Palestiniens.


- :ref:`ÉPISODE 1 La Palestine, une terre deux fois promise <barthe_1_sur_5_2024_01_07>`
- :ref:`ÉPISODE 2 La Nakba, grande déchirure de la Palestine <barthe_2_sur_5_2024_01_08>`
- :ref:`ÉPISODE 3 A Jérusalem, en 1967, le retour du religieux <paris_3_sur_5_2024_01_09>`
- :ref:`ÉPISODE 4 De l’espoir d’Oslo aux réalités d'Hébron <paris_4_sur_5_2024_10_10>`
- :ref:`ÉPISODE 5 En Israël, de la promesse de concorde à l’incendie identitaire <smolar_5_sur_5_2024_01_12>`
