.. index::
   pair: Ressources ; Israël-Palestine

.. _ressources:

==========================================================
**Ressources sur les conflits Israël-Palestine**
==========================================================


I Rapports et communiqués des organisations gouvernementales et cours de justice
=======================================================================================

Israel’s 55-year occupation of Palestinian Territory is apartheid - UN human rights expert
-------------------------------------------------------------------------------------------------

- https://www.ohchr.org/en/press-releases/2022/03/israels-55-year-occupation-palestinian-territory-apartheid-un-human-rights  (Lien vers le rapport en français)

Statement of ICC Prosecutor Karim A.A. Khan KC: Applications for arrest warrants in the situation in the State of Palestine
-------------------------------------------------------------------------------------------------------------------------------

- https://www.icc-cpi.int/news/statement-icc-prosecutor-karim-aa-khan-kc-applications-arrest-warrants-situation-state 

Destruction et violence à Gaza : un avenir incertain pour les enfants de la région (UNICEF)
-------------------------------------------------------------------------------------------------

- https://www.unicef.fr/article/destruction-et-violence-a-gaza-un-avenir-incertain-pour-les-enfants-de-la-region/ 


II Rapports et communiqués des ONG
=============================================

Rapport de B’tselem sur la torture dans les prisons israéliennes 
------------------------------------------------------------------

- https://www.btselem.org/publications/202408_welcome_to_hell 

Rapport de B’tselem sur la famine à Gaza 
----------------------------------------------

https://www.btselem.org/publications/202404_manufacturing_famine

Analyse du 7 octobre par Human Rights Watch 
-----------------------------------------------

- https://www.hrw.org/fr/news/2024/07/17/le-temps-compte-comment-hrw-etablit-une-chronologie-en-analysant-des-videos 

Israël : Des professionnels de santé palestiniens ont été torturés (Communiqué de Human Rights Watch)
---------------------------------------------------------------------------------------------------------

- https://www.hrw.org/fr/news/2024/08/26/israel-des-professionnels-de-sante-palestiniens-ont-ete-tortures 

Destructions massives et injustifiées à Gaza : une enquête sur les crimes de guerre doit être ouverte (Amnesty International)
----------------------------------------------------------------------------------------------------------------------------------

- https://www.amnesty.fr/conflits-armes-et-populations/actualites/destructions-massives-et-injustifiees-a-gaza-enquete-crime-de-guerre 



