

.. _conflit_israel_1949_1979:

============================================================================================
1949 **Les guerres israélo-arabes et l'expansion d'Israël : 1949-1979** par Osons causer
============================================================================================

- https://www.youtube.com/watch?v=p2YA6-ades8
- :ref:`osons_comprendre_2023_12_23`


.. youtube:: p2YA6-ades8


À sa création en 1948, Israël est un petit pays, menacé par ses voisins 
arabes. 

En 1979, l'État hébreu est une puissance militaire et nucléaire incontestée, 
soutenu par les États-Unis en paix avec plusieurs de ses voisins. 

Toutes les sources de la vidéo sont en accès libre sur notre site : 
https://www.osonscomprendre.com/video/les-guerres-israelo-arabes-et-lexpansion-disrael-1949-1979/


Comment les guerres israélo-arabes ont-elles permis à Israël d'asseoir 
sa domination sur la région ?

La première partie de notre mini série historique déjà sur Youtube https://www.osonscomprendre.com/video/arafat-et-lolp-de-la-lutte-armee-a-la-negociation-la-resistance-palestinienne-en-exil-1949-1982/


La troisième partie, Arafat et l’OLP, de la lutte armée à la négociation. 
La résistance palestinienne en exil (1949-1982), est disponible sur 
Osons Comprendre : https://www.osonscomprendre.com/video/arafat-et-lolp-de-la-lutte-armee-a-la-negociation-la-resistance-palestinienne-en-exil-1949-1982/
