.. index::
   pair: 1967 ; A Jérusalem, en 1967, le retour du religieux (Gilles Paris)

.. _paris_3_sur_5_2024_01_09:

======================================================================================================================================
2024-01-09 Série "Israël-Palestine, la guerre sans fin" (3/5) **A Jérusalem, en 1967, le retour du religieux** par Gilles Paris
======================================================================================================================================


- https://www.lemonde.fr/international/article/2024/01/09/a-jerusalem-en-1967-le-retour-du-religieux_6209894_3210.html
- https://www.lemonde.fr/signataires/gilles-paris/

A Jérusalem, en 1967, le retour du religieux
=================================================

Série « Israël-Palestine, la guerre sans fin » (3/5). La prise de la Vieille
Ville de Jérusalem et les conquêtes de Gaza et de la Cisjordanie par l’armée
israélienne lors de la guerre des Six-Jours épousent les aspirations des
sionistes religieux, partisans du Grand Israël, et radicalisent aussi le
camp palestinien.

Le 7 juin 1967, en début d’après-midi, le ministre israélien de la défense,
Moshe Dayan, pénètre dans la Vieille Ville de Jérusalem par la porte des
Lions, située à l’est, face au mont des Oliviers. La deuxième guerre
israélo-arabe n’a que deux jours, mais déjà l’armée égyptienne a été
balayée dans le Sinaï par l’offensive surprise de l’Etat hébreu. Quant
à l’armée jordanienne, elle reflue de toutes parts. Encore deux jours,
et Israël se tournera vers le Golan syrien pour y mettre en déroute, à leur
tour, les forces de Damas.

Venu en toute hâte de Tel-Aviv, par hélicoptère, Moshe Dayan s’avance dans
les rues étroites, accompagné par le chef d’état-major, Yitzhak Rabin, avant
de bifurquer sur sa gauche. Quelques instants plus tard, il se retrouve devant
le « Mur occidental » ou « mur des Lamentations » (le Kotel en hébreu),
dont les parachutistes israéliens ont pris le contrôle sans guère combattre,
le matin à 10 heures.

Sans doute le photographe israélien David Rubinger a-t-il déjà fixé sur
sa pellicule l’exubérance du rabbin Shlomo Goren, plus haut responsable
religieux de l’armée (il a le grade de général), soufflant dans sa corne
de bélier, juché sur les épaules d’un militaire. Il a surtout immortalisé
trois soldats extatiques face à la muraille interdite depuis la division de
Jérusalem, en 1949, et donné un négatif de cette photo à un porte-parole
de l’armée, qui la diffuse immédiatement. L’image fera le tour du monde.

Arrivé au pied du mur, Moshe Dayan prend brièvement la parole. « Ce matin,
les forces de défense israéliennes ont libéré Jérusalem. Nous avons
unifié Jérusalem, la capitale divisée d’Israël. Nous sommes revenus dans
le plus saint de nos lieux saints, pour ne plus jamais nous en séparer »,
assure-t-il devant les impressionnants blocs de pierre blonde qui constituent,
selon la tradition juive, les seuls vestiges du second temple détruit par
les légions de Titus, en l’an 70 de notre ère.

Le rabbin Shlomo Goren y conduit alors les premières prières juives depuis
deux décennies. Il nourrit aussi le projet, vite bloqué par Moshe Dayan, de
dynamiter le dôme du Rocher, l’une des deux mosquées (avec celle d’Al-Aqsa,
troisième lieu saint de l’islam), érigées sur l’esplanade en surplomb
du mur.

Le quartier maghrébin rasé
===============================

Israël vient de balayer les certitudes qui prévalaient encore à la veille
du conflit. Deux jours plus tard, c’est littéralement que l’Etat hébreu
fera table rase, en faisant disparaître le quartier maghrébin, voisin de
l’esplanade, dont l’historien Vincent Lemire a raconté, à la manière
d’un enquêteur, la brutale oblitération (Au pied du Mur, Seuil, 2022). En
vingt-quatre heures, sans la moindre annonce officielle, une centaine de
maisons sont rasées et leurs gravats évacués. Des dizaines de milliers
d’Israéliens se pressent les jours suivants sur ce qui est devenu, en une
poignée d’heures, une vaste place.

La guerre produira bien d’autres ruines, cette fois-ci géopolitiques. La
« ligne verte » qui séparait l’Etat hébreu des territoires palestiniens
contrôlés par l’Egypte et la Jordanie depuis 1949, à savoir Gaza et
la Cisjordanie, a été emportée. Bientôt elle disparaîtra des cartes
israéliennes. Le panarabisme incarné au Caire par Gamal Abdel Nasser ne
se relèvera pas de l’humiliation. Lorsque les armes se taisent, le 10
juin, l’heure est à l’euphorie. Les Israéliens, qui craignaient pour
l’existence même de leur Etat, peuvent s’enivrer de cette victoire à
plate couture.

Le 6 juin déjà, à la veille de la prise de la Vieille Ville, le cabinet
israélien s’était réuni pour tirer les premières leçons de la
guerre. Alors que les discussions précédant l’attaque israélienne
s’étaient surtout concentrées sur l’Egypte et l’attitude de
l’indispensable allié américain, le cours des combats offrait la perspective
d’une surprise divine : le parachèvement de la conquête de la terre biblique
d’Israël, de la mer à la rivière, le Jourdain.

Deux camps s’opposaient : les politiques, dirigés par le premier ministre
Levi Eshkol, successeur de David Ben Gourion et membre comme lui du Mapaï,
la gauche séculariste qui a forgé Israël, et les militaires. Ces derniers
sont soutenus par Menahem Begin, le responsable encore très minoritaire du
parti nationaliste Hérout, héritier du parti révisionniste de Vladimir
Jabotinsky, avocat virulent d’un Grand Israël. Il a été invité à
rejoindre un gouvernement d’union nationale avant le premier coup de feu.

L’affrontement a été décrit par l’historien israélien Mordechai
Bar-On comme une « révolte des généraux », qui souhaitaient passer à
l’action alors que Levi Eshkol et d’autres ministres plaidaient pour
l’attentisme. Le 2 juin, Levi Eshkol avait ainsi lancé aux va-t-en-guerre
: « Une victoire militaire ne mettra pas fin au problème car les Arabes ne
vont pas disparaître. »

Mais dès lors que les villes palestiniennes de Cisjordanie tombent les unes
après les autres, sans combats ou presque, les généraux, conduits par
Moshe Dayan, entendent pousser leur avantage et tirer profit d’une aubaine :
l’entrée en guerre précipitée de la Jordanie, qui va s’avérer tragique
pour le roi Hussein.

Moshe Dayan, ministre de la défense israélien, entouré d’Yitzhak Rabin,
chef d’état-major (à droite) et Uzi Narkiss, responsable militaire de la
région centre (à gauche), entre dans la Vieille Ville de Jérusalem par la
porte des Lions, le 7 juin 1967. ILAN BRUNER / REUTERS

Ce 6 juin, les deux camps ne sont donc pas de même force. Selon le récit
dressé par l’historien Tom Segev, le propre conseiller militaire de Levi
Eshkol, Israël Lior, estime qu’« il était clair pour tous les ministres
et tous les généraux que la roue de la guerre ne pouvait plus faire marche
arrière ». Moshe Dayan a retardé la conquête faute de savoir quoi en faire,
mais il est clair que la prise de la Vieille Ville de Jérusalem n’est plus
qu’une question d’heures. Problème : rien n’a été prévu pour le
jour d’après.

Le gouvernement dépassé par les conquêtes de l’armée
=========================================================

Selon l’historien Avi Shlaim, le 26 mai, au cours d’une réunion de
l’état-major, Yitzhak Rabin avait écarté toute velléité expansionniste,
si la guerre tournait à l’avantage d’Israël. La position du gouvernement,
a-t-il rappelé, est de considérer d’éventuels gains territoriaux comme
une monnaie d’échange en vue d’un futur accord de paix, comme ce sera
le cas une décennie plus tard avec le Sinaï et l’Egypte, conformément à
la résolution 242 des Nations unies, adoptée en novembre 1967, qui souligne
« l’inadmissibilité de l’acquisition de territoires par la guerre » et
demande le « retrait des forces armées israéliennes des territoires occupés
au cours du récent conflit ».

En juin 1967, la dynamique de la guerre bouscule le gouvernement de Levi
Eshkol. Comme le confiera plus tard le responsable militaire de la région centre
Uzi Narkiss, « nous avons agi si rapidement que le gouvernement israélien
n’a pas eu le temps de déterminer les objectifs nationaux de cette guerre
». « Le gouvernement aurait pu nous ordonner de ne pas prendre Ramallah. Il
aurait pu nous ordonner de ne pas prendre Jéricho, ou de nous arrêter avant
Bethléem, ou avant Hébron, ou avant Naplouse. Ils ne l’ont pas fait parce
que je pense qu’ils étaient dépassés par la situation et qu’ils n’ont
pas eu le temps de dire stop », précisera-t-il. La défiance à l’égard du
premier ministre manifestée par le ministre de la défense Moshe Dayan, qui,
selon Tom Segev, s’est parfois dispensé d’obtenir un feu vert préalable
avant certaines actions, n’a pu que compliquer la situation.

Le 7 juin au soir, le chef d’état-major israélien prend brusquement la mesure
des implications de cet emballement. Selon le journaliste Abraham Rabinovitch,
Yitzhak Rabin pose la question soigneusement évitée jusqu’à présent :
« Comment contrôler un million d’Arabes ? » « Un million deux cent
cinquante mille », rectifie un adjoint. Dans les semaines qui suivent la
victoire, rapporte Avi Shlaim, le premier ministre israélien arbore un «
V » churchillien. A son épouse qui s’interroge, Levi Eshkol répond avec
humour : « Non, ce n’est pas un signe V en anglais, c’est un signe V en
yiddish ! Vi krikht men aroys ? », qu’on peut traduire par « comment s’en
sortir ? » (The 1967 Arab-Israeli War : Origins and Consequences, non traduit,
Cambridge University Press, 2012).

Jusqu’alors, les autorités israéliennes se sont calées sur les principes
de David Ben Gourion, qui préférait « un petit Israël avec la paix » à
« un Grand Israël sans la paix ». Sa traduction concrète est la formule de
« la terre contre la paix », rappelée par Yitzhak Rabin. Si la conquête
des territoires palestiniens prend de court le Mapaï, elle électrise au
contraire le Parti national religieux (PNR), une force d’appoint de longue
date dont les positions ont été jusqu’à présent assez modérées.

Le PNR a ainsi fait sien le modèle du kibboutz, signe de l’hégémonie
culturelle de la gauche, en y ajoutant simplement une dimension religieuse. En
politique étrangère, il soutient lui aussi la formule de la terre contre
la paix. Dans les semaines qui ont précédé la guerre, l’un de ses chefs
de file, Haim-Moshe Shapira, inamovible ministre chargé à cet instant des
affaires intérieures, a longtemps plaidé pour la prudence, comme le premier
ministre Levi Eskhol. « Je reconnais que j’ai été lâche », avoue-t-il
lors de la réunion cruciale du 6 juin qui précède la conquête de la Vieille
Ville de Jérusalem.

« Messianisme territorial »
================================

A cet instant, le Parti national religieux est l’incarnation d’« une
tentative de dépassement », selon le chercheur David Khalfa, dans une note
publiée en 2005 par le Centre de recherche français à Jérusalem. Ce parti
rejette en effet aussi bien « le quiétisme traditionnel de l’ultraorthodoxie
religieuse et sa condamnation radicale du sionisme comme œuvre impie » que «
l’idéologie prométhéenne de la gauche sioniste socialiste qui entendait
faire table rase du passé “diasporique” et favoriser l’émergence
d’un nouvel “Hébreu” détaché de la tradition juive et de la Torah
». Le triomphe de 1967 et du soldat-kibboutznik témoigne de la puissance de
cette gauche sioniste. Mais celle-ci reste sans voix devant les conséquences
qu’il entraîne pour la Cisjordanie.

Au contraire, ce triomphe amorce « une mutation lente mais réelle du sionisme
religieux avec la montée en puissance au sein du PNR d’activistes jusqu’à
présent marginalisés, une jeune génération habitée par un messianisme
territorial », selon le spécialiste du sionisme religieux Alain Dieckhoff,
directeur du Centre de recherches internationales de Sciences Po. Moins d’un
mois après la victoire, un courant ultranationaliste se constitue au sein du
parti. Il se donne le nom de Jeune Garde (« Ha Tzeirim ») et s’appuie sur
le puissant patronage du rabbin Zvi Yehuda Kook (1891-1982).

Fils d’Abraham Isaac Kook (1865-1935), premier grand rabbin ashkénaze du
Yichouv, le « foyer juif » de Palestine à l’époque du mandat britannique,
Zvi Yehuda Kook est convaincu que les sionistes non religieux, par leur projet
politique, précipitent involontairement la marche vers la rédemption et
la délivrance messianique. Il dirige la yeshiva Merkaz HaRav fondée par son
père, devenue le creuset idéologique, selon Alain Dieckhoff, d’un messianisme
irrédentiste électrisé par la guerre de 1967. Il rappelle que quelques jours
avant le début de la guerre, « le rabbin Kook prononce une sorte de sermon
dans lequel il regrette le fait que la Judée Samarie [appellation biblique
de la Cisjordanie] ne soit pas sous le contrôle d’Israël parce que cela
aurait une portée religieuse forte ».

Après la conquête de la Vieille Ville de Jérusalem, le 7 juin 1967, pendant
la guerre des Six-Jours, un soldat israélien efface une inscription en arabe
sur le mur des Lamentations. MICHA BAR-AM / MAGNUM

Trois mois plus tard, à la fin du mois de septembre, une douzaine
d’Israéliens s’installent en Cisjordanie, au sud de Jérusalem, sur le
site de Kfar-Etzion, une ancienne implantation juive érigée quelques années
avant la première guerre israélo-arabe de 1948-1949 et détruite dans le
sang durant les combats. Encore toute à son ivresse de la victoire de juin,
la presse israélienne célèbre ce « retour », au moment même où l’Etat
hébreu interdit celui des Palestiniens qui ont fui les combats en Jordanie.

« Au-delà de cette simple reprise de possession, il y a la conviction
qu’il s’est passé quelque chose de l’ordre du miracle avec la guerre
de Six-Jours. Elle n’apparaît pas seulement à cette jeune génération
sioniste religieuse comme une victoire militaire, mais elle comporte également
à leurs yeux une dimension religieuse avec la récupération des hauts lieux
de la mémoire juive. C’est ce qui fait qu’il est impératif pour eux de
développer une présence juive en “Judée Samarie” », explique Alain
Dieckhoff.

Ces nouveaux « pionniers » sont soutenus par les ministres du Parti national
religieux et par le premier ministre Levi Eshkol. On trouve parmi eux des
élèves de la yeshiva du Merkaz HaRav, dont les rabbins Hanan Porat et Moshe
Levinger, qui deviennent les fers de lance d’une colonisation qui s’avance
encore masquée. Kfar-Etzion est en effet faussement présenté comme un simple
poste militaire.

Sept années s’écoulent encore, et un nouveau mouvement sioniste religieux
est créé par un autre ancien du Merkaz HaRav, le rabbin Haïm Druckman. Le
Goush Emounim (« Bloc de la foi ») fait de la colonisation des territoires
occupés en 1967 une obligation qui va s’imposer à toutes les coalitions
gouvernementales israéliennes, de droite comme de gauche, qu’elles soutiennent
des négociations de paix avec les Palestiniens ou qu’elles s’y opposent,
et cela jusqu’à aujourd’hui.Le sionisme religieux a définitivement
rompu avec son pragmatisme, et son programme se résume désormais à la
revendication d’une souveraineté juive sur l’intégralité de la terre,
à commencer par la partie orientale de Jérusalem, objet d’une sourde
bataille de réappropriation, maison par maison. Une page se tourne. La
dernière guerre israélo-arabe de 1973, qui prend de court les autorités
israéliennes, acte le déclin de la gauche. En 1977, elle sera battue pour
la première fois dans les urnes, par la droite nationaliste de Menahem Begin
que rallie immédiatement le PNR.

Un lieu saint, thermomètre des tensions
============================================

A ces mutations israéliennes engagées en 1967 font écho celles qui
travaillent le camp palestinien. La déroute des régimes arabes laisse les
coudées franches à un courant nationaliste incarné par Yasser Arafat et
par le Fatah, initialement aux marges de l’Organisation de libération
de la Palestine constituée en 1964 dans la partie orientale de Jérusalem,
sous les auspices des puissances égyptiennes, syriennes et jordaniennes. Sa
Charte publiée la même année témoigne d’une vision aussi intégrale de la
terre que celle des sionistes religieux les plus radicaux. Yasser Arafat prend
le contrôle de la centrale palestinienne en 1969 en agitant alternativement
Le Rameau d’olivier et le fusil du combattant, selon le titre du tome IV
(Fayard, 2011) de l’histoire monumentale consacrée par Henry Laurens à La
Question de Palestine, qui renvoie à l’après-1967.

En reprenant pied par surprise dans le Sinaï en 1973 lors de la dernière
guerre israélo-arabe, le successeur de Nasser, Anouar El-Sadate, obtient une
revanche que n’annule pas la violente contre-attaque israélienne. Mais les
temps ont changé, et il n’a plus comme objectif que de restaurer l’image
de l’Egypte pour préparer une paix séparée avec Israël. Le front arabe
a vécu. Certes, le sort des Palestiniens est spécifiquement mentionné dans
les accords de Camp David, en 1978. Il n’empêche : celui-ci est passé par
pertes et profits.

Le retour du religieux, engagé en Israël à partir de 1967, n’épargne pas
le camp palestinien. A la fin des années 1970, les Frères musulmans renoncent
au quiétisme pour progressivement s’engager dans le combat politique, en
Egypte et ailleurs. A Jérusalem, l’esplanade des Mosquées – le mont du
Temple pour les juifs – devient un thermomètre des tensions. En 1990, elle
sera la cible de juifs messianiques voulant, comme le rabbin Shlomo Goren en
1967, raser les deux mosquées pour ériger à leur place le Troisième Temple.

En 1996, le creusement d’un tunnel archéologique à proximité de
l’esplanade des Mosquées déclenche des émeutes sanglantes dans les
territoires palestiniens. Après l’échec, en juillet, des négociations
de Camp David, notamment sur la question de l’esplanade des Mosquées,
une visite sur les lieux d’Ariel Sharon, chef de la droite nationaliste
désormais représentée par le Likoud, sonne le début de la deuxième Intifada
en 2000. Itamar Ben Gvir, chef de file d’un sionisme religieux désormais
installé au cœur du gouvernement israélien, s’y rend à trois reprises
avant la guerre déclenchée par les massacres de masse du Hamas, le 7 octobre,
baptisés « Déluge d’Al-Aqsa ».

Il n’est plus de guerre israélo-palestinienne, désormais, qui ne plonge
ses racines à Jérusalem.

Les 5 épisodes
======================

De la naissance du sionisme, sur fond de persécutions antijuives en Europe
et d’agonie de l’Empire ottoman, à la montée, au début du XXIe siècle,
d’un antagonisme rendant très improbable le rêve d’une solution à deux Etats,
"Le Monde"revient, dans une série de cinq articles produits par d’anciens
correspondants dans la région, sur l’histoire de la guerre sans fin qui
oppose Israéliens et Palestiniens.


- :ref:`ÉPISODE 1 La Palestine, une terre deux fois promise <barthe_1_sur_5_2024_01_07>`
- :ref:`ÉPISODE 2 La Nakba, grande déchirure de la Palestine <barthe_2_sur_5_2024_01_08>`
- :ref:`ÉPISODE 3 A Jérusalem, en 1967, le retour du religieux <paris_3_sur_5_2024_01_09>`
- :ref:`ÉPISODE 4 De l’espoir d’Oslo aux réalités d'Hébron <paris_4_sur_5_2024_10_10>`
- :ref:`ÉPISODE 5 En Israël, de la promesse de concorde à l’incendie identitaire <smolar_5_sur_5_2024_01_12>`
