.. index::
   pair: 2015 ; En Israël, de la promesse de concorde à l’incendie identitaire (Piotr Smolar)


.. _smolar_5_sur_5_2024_01_12:

===============================================================================================================================================
2024-01-12 "Israël-Palestine, la guerre sans fin"(5/5) **En Israël, de la promesse de concorde à l’incendie identitaire** Par Piotr Smolar 
===============================================================================================================================================

- https://www.lemonde.fr/international/article/2024/01/11/en-israel-de-la-promesse-de-concorde-a-l-incendie-identitaire_6210247_3210.html

Auteur Piotr Smolar 
======================

- https://www.lemonde.fr/signataires/piotr-smolar/

En Israël, de la promesse de concorde à l’incendie identitaire
=================================================================

Construit sur un engagement de liberté, de justice et de paix pour tous 
ses habitants, l’Etat hébreu a évolué au fil des années vers une idéologie 
extrémiste, rendant chaque jour plus illusoire le rêve de la solution 
à deux Etats.

On a demandé aux invités de taire la raison de leur venue. En vain. C’est le
secret le moins bien préservé du moment. Les voitures officielles se succèdent
devant le Musée d’art de Tel-Aviv. Il est 16 heures. Il faut se dépêcher
avant le début du shabbat. Alors que le mandat britannique en Palestine est
sur le point d’expirer, en ce vendredi 14 mai 1948, plusieurs centaines de
personnes se sont rassemblées au pied du bâtiment, retenant leur souffle.

A l’intérieur, les visages sont graves. Sous un portrait intimidant du
journaliste austro-hongrois Theodor Herzl, le père du sionisme politique,
David Ben Gourion (1886-1973) lit la déclaration d’indépendance du nouvel
Etat devant les représentants de toutes les composantes de la communauté juive.

Ses termes ont fait l’objet d’âpres négociations. En particulier la
référence à Dieu. Trop présente pour les laïques, puis expurgée du texte à
la colère des religieux, et enfin réintroduite sous une forme discrète. Tzur
Yisrael – "le rocher d’Israël"– est une expression commode car
ambivalente. On peut la saisir comme une référence au créateur dans le
judaïsme mais aussi au peuple juif, d’un point de vue politique. La voix
de David Ben Gourion ne tremble pas. Elle est néanmoins gonflée d’émotion
au moment de lire la déclaration devant les micros.

C’est une promesse volontariste en des temps bien incertains, faite au nom
des morts de la Shoah, en réponse à deux mille ans d’exil et pour les
générations à venir. Le texte proclame "la création d’un Etat juif en
terre d’Israël". Fondé sur la "liberté, la justice et la paix", il
consacre le développement du pays "pour le bénéfice de tous ses habitants
» et assure "la plus complète égalité sociale et politique", cela «
sans distinction de religion, de race ou de sexe". Egalité, absence de toute
discrimination : les racines socialistes de l’Etat apparaissent clairement.

Il est 4 heures ce 19 juillet 2018
=========================================

Nous voilà soixante-dix ans plus tard. Israël est à l’âge adulte, ce qui
n’empêche pas les crises existentielles. Il est 4 heures ce 19 juillet 2018,
et les lumières ne sont toujours pas éteintes à la Knesset, le Parlement
israélien. Voici venu le temps d’un selfie. On est loin du noir et blanc qui
avait immortalisé David Ben Gourion. Ce cliché réunit des élus du Likoud,
la formation principale de la droite, autour de leur chef de file, le premier
ministre Benyamin Nétanyahou. 

Que fêtent-ils ainsi ? 

L’adoption d’un texte historique et controversé, par 62 voix contre 55, 
définissant Israël comme "le foyer national du peuple juif". 

Une expression commune. Le contexte ne l’est pas.

Confettis
===========

Le député qui tient le téléphone à bout de bras, hilare, s’appelle Oren
Hazan. Il porte une chemise blanche à manches courtes et un jean. On voit son
ventre et son caleçon. Les selfies sont l’une de ses spécialités. En mai
2017, il avait provoqué un scandale sur le tarmac de l’aéroport Ben-Gourion
au pied d’Air Force One, l’avion présidentiel américain, en recueillant
ainsi un cliché aux côtés de Donald Trump à son arrivée en Israël.

Depuis son entrée à la Knesset, en 2015, Oren Hazan a été régulièrement
blâmé, suspendu, expulsé. La presse a révélé son passé comme tenancier de
casino en Bulgarie, encourageant la drogue et la prostitution.  Mais, en ce 19
juillet, qui se soucie des excentricités d’Oren Hazan ? Il s’agit alors,
selon Benyamin Nétanyahou, d’un "moment charnière dans les annales du
sionisme et de l’Etat d’Israël". Dans les travées de l’opposition, la
colère et l’effroi dominent. "Je déclare avec stupéfaction et tristesse
la mort de la démocratie", affirme Ahmad Tibi, tandis que son collègue de
la Liste arabe unie, Jamal Zahalka, fait des confettis avec le texte.

Le patron de l’Agence juive, Natan Sharansky, le procureur général, Avichai
Mandelblit, et même le président israélien, Réouven Rivlin, avaient exprimé
de vives préoccupations au sujet de la première mouture. Selon le chef de
l’Etat, un article pouvait causer un grave préjudice à la réputation
d’Israël. Il offrait la possibilité à un groupe, représentant une
population ou un culte spécifique, d’établir une communauté d’habitation
séparée. Soit une carte blanche aux discriminations, au nom d’une
homogénéité recherchée et assumée.  Selon le président israélien, le
texte, dépourvu de limitations, aurait permis d’établir "une communauté
sans juifs mizrahim, sans ultraorthodoxes, sans Druzes, sans membres LGBT". Au
cours de négociations finales, Nétanyahou et son ministre de l’éducation,
Naftali Bennett, représentant du parti messianique Foyer juif, décident
d’abandonner cette clause. La formule retenue considère le développement
de communautés juives comme une "valeur nationale", sans préciser si
c’est en Israël même ou en Cisjordanie occupée. Un flou calculé.

Depuis sept ans, l’idée d’inscrire la définition d’Israël comme Etat
juif dans une Loi fondamentale – la plus haute valeur juridique qui soit, en
l’absence de Constitution – revenait de façon régulière à droite. Fin
novembre 2014, Benyamin Nétanyahou s’exprimait déjà à la tribune de la
Knesset pour défendre sa propre version du projet, souvent revisité. "Au
fil des ans, expliquait-il, un déséquilibre clair s’est créé entre le
caractère juif et démocratique [de l’Etat]. Il y a un déséquilibre entre
les droits individuels et les droits nationaux en Israël."

Message symbolique

Un péril identitaire pèserait donc sur Israël car sa dimension nationale
juive ne serait pas cultivée et célébrée. Quatre ans plus tard, le
débat ressurgit, toujours aussi vigoureux. Cette fois, le premier ministre
a décidé de promouvoir le projet de loi juste avant la fin de la session
parlementaire. Les élections approchent. "Bibi"veut offrir un cadeau
d’envergure aux nationalistes et à l’extrême droite. Il est un peu
fébrile : depuis un an et demi, il a été interrogé à dix reprises par la
police dans plusieurs enquêtes judiciaires qui mettent en cause sa probité.
Le texte final voté à la Knesset est-il purement symbolique et déclaratif,
ou bien révèle-t-il quelque chose de plus profond sur l’évolution du
pays ? Bien qu’édulcorée par rapport aux ambitions premières du camp
nationaliste, la 14e Loi fondamentale adoptée ce jour de juillet 2018 est
une borne identitaire. Elle rappelle de nombreux éléments figurant déjà
dans la déclaration d’indépendance et fixe les attributs de souveraineté,
comme le drapeau, l’hymne national, le calendrier juif.

Elle définit aussi Jérusalem comme la capitale "complète et unifiée
» d’Israël, alors que le statut de la Ville sainte ne devrait être
tranché qu’au terme de négociations de paix avec les Palestiniens, en
vertu du consensus international. Le texte accorde aux seuls Juifs le droit à
l’autodétermination.  Cette insistance s’accompagne d’une dégradation,
celle de la langue arabe. Elle cesse d’être langue officielle au même titre
que l’hébreu, et se voit dorénavant attribuer un "statut spécial". Cela
change-t-il quelque chose dans les faits ? Le problème réside dans le message
symbolique, la menace implicite adressée à la minorité arabe. Ces citoyens,
qui disposent en principe de droits civiques pleins, paient des impôts et
votent aux élections, peuvent s’interroger sur leur place dans la société
israélienne.  Une rupture d’égalité – un principe au cœur de la promesse
de 1948, oublié dans la nouvelle loi – apparaît clairement. Mais elle existe
déjà en pratique. Les hommes ultraorthodoxes, par exemple, ne sont-ils pas
exemptés de service militaire obligatoire, offrant leur temps à Dieu plutôt
qu’à la défense de la patrie ? Les jeunes couples ne sont-ils pas soumis,
au moment du mariage, au monopole religieux du grand rabbinat, alors que le
mariage civil n’existe pas ?

Racines enroulées sur les pierres

La loi de 2018 révèle en réalité les tourments identitaires d’Israël et
l’introuvable définition de son Etat. La permanence de menaces extérieures
à ses frontières ne peut tenir lieu de seul ciment. D’abord, ses frontières
sont incertaines. Au nord, l’annexion du plateau syrien du Golan n’est pas
reconnue sur le plan international. A l’est, l’occupation en Cisjordanie
– un régime supposément provisoire depuis la guerre des Six-Jours en
1967, muant en annexion rampante – crée un flou existentiel. Où finit
Israël ? La question n’est pas que territoriale mais aussi démographique.
En ne définissant pas son territoire, Israël s’expose à des projections
menaçantes. En avril 2023, le Bureau central des statistiques estimait que
la population totale dans le pays s’élevait à 9,7 millions de personnes,
dont 7,1 millions de Juifs (73,5 %) et 2,04 millions d’Arabes (21 %). Mais si
on ajoute la population palestinienne en Cisjordanie et à Gaza, la majorité
juive devient rachitique, voire inexistante.

Dès lors, le dilemme est clair. Comment rester à la fois un Etat juif et
démocratique tout en conservant la mainmise sur les territoires obtenus en 1967
? La domination ou le droit ? La terre ou l’âme ? La plasticité identitaire
est au cœur même du judaïsme, par ses mille nuances possibles. Mais que
faire si une lecture restrictive s’impose, plaçant Dieu à la tête d’un
nouveau plan cadastral ?  Ce déchirement identitaire est le fil conducteur de
l’histoire d’Israël depuis l’assassinat du premier ministre Yitzhak Rabin
par un extrémiste juif en novembre 1995, puis la campagne d’attentats-suicides
palestiniens, pendant la seconde Intifada (2000-2005). Ces deux séquences
ont éteint la promesse d’Oslo, les accords de paix conclus en 1993, qui
dessinaient la voie vers la solution à deux Etats. L’assassinat de Rabin
a révélé que la bataille pour l’âme d’Israël, pour sa définition,
était une affaire de vie et de mort.  Il y a, à cette époque, environ
140 000 colons en Cisjordanie, contre près de 500 000 aujourd’hui, qui
vivent dans 146 colonies et 144 avant-postes, selon l’organisation La Paix
maintenant. Ils ont gagné sur la durée. Leurs racines s’enroulent autour
des pierres, de colline en colline. Imaginer qu’on puisse les déloger,
par l’incitation financière ou la force, est une illusion.

Se soumettre ou partir
==========================

Depuis le début des années 1970, les colons messianiques portent un récit,
celui d’une reconquête de la "Judée-Samarie", d’un retour aux
terres bibliques. D’abord marginal et sectaire, il n’a cessé de gagner
en légitimité, en centralité. D’autant que la gauche travailliste est
devenue atone. Cela s’explique d’abord par l’onde de choc de la seconde
Intifada. Celle-ci a imposé un profond pessimisme au sein de la société,
bien au-delà des lignes partisanes, sur la possibilité d’une paix. En
résumé, il n’y aurait personne avec qui négocier.  Dès lors, l’Etat
hébreu assume seul tous les choix stratégiques. C’est ainsi que commence
la gestion du conflit à feu réduit mais permanent. Il ne s’agit pas de
trouver une solution, mais de dominer l’adversaire, de prévenir ou de punir
ses explosions de violence.

La première étape a été le retrait de la bande de Gaza, en 2005. Une
décision historique, prise par le premier ministre de l’époque, Ariel Sharon,
sans concertation avec l’Autorité palestinienne (AP).  Plusieurs milliers
de soldats israéliens protégeaient dans l’enclave quelque 8 500 colons, qui
opposèrent pour certains une farouche résistance à cette évacuation. Sharon
était un faucon aux calculs complexes. Dans une lettre au président américain
d’alors, George W. Bush, il constatait "l’impasse"dans les relations
avec l’AP. Yasser Arafat, son dirigeant, était mort un an plus tôt et son
parti, le Fatah, semblait affaibli. Le retrait israélien de Gaza ouvre dès
lors la porte du pouvoir au Hamas, qui va obtenir des résultats inattendus
aux élections de 2006, puis régler ses comptes par les armes avec le Fatah
et s’accaparer tous les pouvoirs dans l’enclave à partir de 2007. Gaza
se transforme petit à petit en cellule aux barreaux épais.

La conclusion logique de cette séquence aurait dû être, pour Israël,
qu’aucun progrès ne peut être accompli vers la paix et la sécurité
sans un partenariat avec l’AP. Au lieu de cela, une amertume du retrait va
s’installer, au fil des cycles d’affrontement suivants avec les factions
armées à Gaza (2008-2009, 2012, 2014, 2021 et 2023). La droite israélienne
a retenu comme leçon que ce retrait fut un cadeau trop coûteux offert aux
Palestiniens. Face à eux, seuls la force ou le déni deviennent des positions
admises.  La question de leurs droits politiques disparaît pour laisser la
place à la promesse conditionnée de simples incitations économiques. Un
double mouvement, en apparence contradictoire, s’opère à droite : on
essentialise ce peuple voisin, dans un élan ouvertement raciste, tout en
niant son existence propre. Simples Arabes parmi les Arabes, les Palestiniens
doivent se soumettre ou partir.

Trump, accélérateur de la dérive
=====================================

**L’élection de Donald Trump aux Etats-Unis agit par la suite comme un puissant
accélérateur de la dérive identitaire israélienne**, au-delà des ressorts
nationaux propres de l’Etat hébreu et de la fragmentation traditionnelle de
la société entre juifs orientaux et européens, centre et périphérie, gauche
et droite.  

L’homme d’affaires devenu président, qui connaît l’importance de la Terre 
sainte pour **l’électorat évangélique américain**, brise tous les
consensus internationaux. 

Il reconnaît Jérusalem comme capitale d’Israël, fin 2017, puis l’annexion 
du plateau du Golan. 
**Il propose un plan de paix en février 2020 qui réalise les rêves les plus 
fous des extrémistes**, sans aller jusqu’à suggérer l’expulsion de tous les 
Palestiniens vers les pays arabes.


L’ivresse identitaire se banalise
=====================================

Il n’y a plus d’administration américaine raisonnable pour retenir la
droite israélienne et justifier des compromis, puisque Donald Trump pilote
lui-même le bulldozer. L’ivresse identitaire se banalise.  

Le parcours de Bezalel Smotrich résume à lui seul la mue de ce camp. 
En 2005, alors jeune extrémiste opposé au désengagement de la bande de Gaza, 
il est arrêté et retenu pendant trois semaines par le Shin Beth, le service 
de sécurité intérieure israélien, car suspecté de vouloir bloquer des routes.

Dix ans après, il entre à la Knesset. 

Depuis décembre 2022, il est ministre des finances au sein du nouveau 
gouvernement Nétanyahou, dominé par l’extrême droite. 

Fier homophobe, opposé à la présence des femmes dans l’armée, défendant 
la supériorité de la Torah sur la loi civile et l’annexion de la Cisjordanie, 
Bezalel Smotrich n’est plus une figure marginale de la droite israélienne. 
Il est sa nouvelle déclinaison.  

A la tribune de la Knesset, en octobre 2021, il est conspué par des députés
arabes. L’élu leur répond : "Vous êtes ici par erreur, ce fut une
erreur de Ben Gourion de ne pas avoir fini le boulot et de ne pas vous avoir
virés en 1948. "Smotrich emploie aussi à leur adresse le mot pivot dans
le lexique de la nouvelle droite israélienne : "ennemis". A compter de
2014, la stigmatisation d’une cinquième colonne fantasmée a pris une place
décisive dans la rhétorique politique.

Il y a d’abord les citoyens arabes d’Israël, que Nétanyahou accuse
d’aller voter "en masse"et "par bus"lors des législatives de
2015. 

Il y a leurs complices supposés, ces organisations non gouvernementales
dites "de gauche", comme Breaking the Silence, B’Tselem ou le New Israel
Fund, qui documentent l’occupation et les guerres à Gaza ou s’émeuvent
des discriminations dans la société israélienne.

Enfin, il y a les journalistes. Ces importuns qui transforment les affres
judiciaires de Nétanyahou en feuilleton, qui racontent ses accointances avec
des milliardaires, son goût des cigares et du champagne, ses arrangements avec
l’éthique.  L’agenda est clair. Briser la résistance palestinienne. Briser
les oppositions internes. Briser les contre-pouvoirs, telle la Cour suprême,
comme le proposait le projet de réforme judiciaire mis en avant par le
gouvernement début 2023. Une mobilisation historique de la société civile
dans la rue a déjoué cette ambition. L’attaque du 7 octobre conduite par
le Hamas a changé les priorités. Deuil, chagrin, désarroi, colère, rage,
vengeance : toute la gamme de ces sentiments a saisi les Israéliens. Union
sacrée ? On peine à distinguer du sacré. Quant à l’union, qui évoque
encore cette ambition ?

En 2015, dans un discours d’une rare envergure, le président Réouven
Rivlin avait décrit un "nouvel ordre israélien". Se défendant de toute
"prophétie apocalyptique", il invitait à regarder "la réalité",
celle de tribus de taille équivalente – ultraorthodoxes, nationalistes
religieux, laïques et Arabes – vivant "dans l’ignorance mutuelle et
une absence de langage commun". Réouven Rivlin, incarnation d’une droite
israélienne classique raisonnable, appelait à construire un "partenariat
» entre ces communautés.

Il disait ceci : "Nous ne sommes pas condamnés à être punis par le
développement de la mosaïque israélienne."

Puis il ajoutait : "Nous ne devons pas laisser le “nouvel ordre israélien” 
nous pousser au sectarisme et à la séparation."

Un avertissement prémonitoire, hélas ignoré.

C’est le drame d’une terre saturée d’histoire et de symboles, théâtre
d’un conflit aux répercussions planétaires. 


Les 5 épisodes
======================

De la naissance du sionisme, sur fond de persécutions antijuives en Europe 
et d’agonie de l’Empire ottoman, à la montée, au début du XXIe siècle, 
d’un antagonisme rendant très improbable le rêve d’une solution à deux Etats, 
"Le Monde"revient, dans une série de cinq articles produits par d’anciens 
correspondants dans la région, sur l’histoire de la guerre sans fin qui 
oppose Israéliens et Palestiniens.


- :ref:`ÉPISODE 1 La Palestine, une terre deux fois promise <barthe_1_sur_5_2024_01_07>`
- :ref:`ÉPISODE 2 La Nakba, grande déchirure de la Palestine <barthe_2_sur_5_2024_01_08>`
- :ref:`ÉPISODE 3 A Jérusalem, en 1967, le retour du religieux <paris_3_sur_5_2024_01_09>`
- :ref:`ÉPISODE 4 De l’espoir d’Oslo aux réalités d'Hébron <paris_4_sur_5_2024_10_10>`
- :ref:`ÉPISODE 5 En Israël, de la promesse de concorde à l’incendie identitaire <smolar_5_sur_5_2024_01_12>`
